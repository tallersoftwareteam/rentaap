package com.taller.web.rest;

import com.taller.RentaapApp;

import com.taller.domain.Suscripcion;
import com.taller.repository.SuscripcionRepository;
import com.taller.service.SuscripcionService;
import com.taller.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.taller.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SuscripcionResource REST controller.
 *
 * @see SuscripcionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentaapApp.class)
public class SuscripcionResourceIntTest {

    private static final Boolean DEFAULT_ESTADO = false;
    private static final Boolean UPDATED_ESTADO = true;

    private static final LocalDate DEFAULT_FECHA_ADQUIRIDA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_ADQUIRIDA = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FECHA_VENCIMIENTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_VENCIMIENTO = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private SuscripcionRepository suscripcionRepository;

    @Autowired
    private SuscripcionService suscripcionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSuscripcionMockMvc;

    private Suscripcion suscripcion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SuscripcionResource suscripcionResource = new SuscripcionResource(suscripcionService);
        this.restSuscripcionMockMvc = MockMvcBuilders.standaloneSetup(suscripcionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Suscripcion createEntity(EntityManager em) {
        Suscripcion suscripcion = new Suscripcion()
            .estado(DEFAULT_ESTADO)
            .fechaAdquirida(DEFAULT_FECHA_ADQUIRIDA)
            .fechaVencimiento(DEFAULT_FECHA_VENCIMIENTO);
        return suscripcion;
    }

    @Before
    public void initTest() {
        suscripcion = createEntity(em);
    }

    @Test
    @Transactional
    public void createSuscripcion() throws Exception {
        int databaseSizeBeforeCreate = suscripcionRepository.findAll().size();

        // Create the Suscripcion
        restSuscripcionMockMvc.perform(post("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suscripcion)))
            .andExpect(status().isCreated());

        // Validate the Suscripcion in the database
        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeCreate + 1);
        Suscripcion testSuscripcion = suscripcionList.get(suscripcionList.size() - 1);
        assertThat(testSuscripcion.isEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testSuscripcion.getFechaAdquirida()).isEqualTo(DEFAULT_FECHA_ADQUIRIDA);
        assertThat(testSuscripcion.getFechaVencimiento()).isEqualTo(DEFAULT_FECHA_VENCIMIENTO);
    }

    @Test
    @Transactional
    public void createSuscripcionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = suscripcionRepository.findAll().size();

        // Create the Suscripcion with an existing ID
        suscripcion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuscripcionMockMvc.perform(post("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suscripcion)))
            .andExpect(status().isBadRequest());

        // Validate the Suscripcion in the database
        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEstadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = suscripcionRepository.findAll().size();
        // set the field null
        suscripcion.setEstado(null);

        // Create the Suscripcion, which fails.

        restSuscripcionMockMvc.perform(post("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suscripcion)))
            .andExpect(status().isBadRequest());

        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaAdquiridaIsRequired() throws Exception {
        int databaseSizeBeforeTest = suscripcionRepository.findAll().size();
        // set the field null
        suscripcion.setFechaAdquirida(null);

        // Create the Suscripcion, which fails.

        restSuscripcionMockMvc.perform(post("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suscripcion)))
            .andExpect(status().isBadRequest());

        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaVencimientoIsRequired() throws Exception {
        int databaseSizeBeforeTest = suscripcionRepository.findAll().size();
        // set the field null
        suscripcion.setFechaVencimiento(null);

        // Create the Suscripcion, which fails.

        restSuscripcionMockMvc.perform(post("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suscripcion)))
            .andExpect(status().isBadRequest());

        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSuscripcions() throws Exception {
        // Initialize the database
        suscripcionRepository.saveAndFlush(suscripcion);

        // Get all the suscripcionList
        restSuscripcionMockMvc.perform(get("/api/suscripcions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(suscripcion.getId().intValue())))
            .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.booleanValue())))
            .andExpect(jsonPath("$.[*].fechaAdquirida").value(hasItem(DEFAULT_FECHA_ADQUIRIDA.toString())))
            .andExpect(jsonPath("$.[*].fechaVencimiento").value(hasItem(DEFAULT_FECHA_VENCIMIENTO.toString())));
    }
    
    @Test
    @Transactional
    public void getSuscripcion() throws Exception {
        // Initialize the database
        suscripcionRepository.saveAndFlush(suscripcion);

        // Get the suscripcion
        restSuscripcionMockMvc.perform(get("/api/suscripcions/{id}", suscripcion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(suscripcion.getId().intValue()))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.booleanValue()))
            .andExpect(jsonPath("$.fechaAdquirida").value(DEFAULT_FECHA_ADQUIRIDA.toString()))
            .andExpect(jsonPath("$.fechaVencimiento").value(DEFAULT_FECHA_VENCIMIENTO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSuscripcion() throws Exception {
        // Get the suscripcion
        restSuscripcionMockMvc.perform(get("/api/suscripcions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSuscripcion() throws Exception {
        // Initialize the database
        suscripcionService.save(suscripcion);

        int databaseSizeBeforeUpdate = suscripcionRepository.findAll().size();

        // Update the suscripcion
        Suscripcion updatedSuscripcion = suscripcionRepository.findById(suscripcion.getId()).get();
        // Disconnect from session so that the updates on updatedSuscripcion are not directly saved in db
        em.detach(updatedSuscripcion);
        updatedSuscripcion
            .estado(UPDATED_ESTADO)
            .fechaAdquirida(UPDATED_FECHA_ADQUIRIDA)
            .fechaVencimiento(UPDATED_FECHA_VENCIMIENTO);

        restSuscripcionMockMvc.perform(put("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSuscripcion)))
            .andExpect(status().isOk());

        // Validate the Suscripcion in the database
        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeUpdate);
        Suscripcion testSuscripcion = suscripcionList.get(suscripcionList.size() - 1);
        assertThat(testSuscripcion.isEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testSuscripcion.getFechaAdquirida()).isEqualTo(UPDATED_FECHA_ADQUIRIDA);
        assertThat(testSuscripcion.getFechaVencimiento()).isEqualTo(UPDATED_FECHA_VENCIMIENTO);
    }

    @Test
    @Transactional
    public void updateNonExistingSuscripcion() throws Exception {
        int databaseSizeBeforeUpdate = suscripcionRepository.findAll().size();

        // Create the Suscripcion

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuscripcionMockMvc.perform(put("/api/suscripcions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suscripcion)))
            .andExpect(status().isBadRequest());

        // Validate the Suscripcion in the database
        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSuscripcion() throws Exception {
        // Initialize the database
        suscripcionService.save(suscripcion);

        int databaseSizeBeforeDelete = suscripcionRepository.findAll().size();

        // Delete the suscripcion
        restSuscripcionMockMvc.perform(delete("/api/suscripcions/{id}", suscripcion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Suscripcion> suscripcionList = suscripcionRepository.findAll();
        assertThat(suscripcionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Suscripcion.class);
        Suscripcion suscripcion1 = new Suscripcion();
        suscripcion1.setId(1L);
        Suscripcion suscripcion2 = new Suscripcion();
        suscripcion2.setId(suscripcion1.getId());
        assertThat(suscripcion1).isEqualTo(suscripcion2);
        suscripcion2.setId(2L);
        assertThat(suscripcion1).isNotEqualTo(suscripcion2);
        suscripcion1.setId(null);
        assertThat(suscripcion1).isNotEqualTo(suscripcion2);
    }
}
