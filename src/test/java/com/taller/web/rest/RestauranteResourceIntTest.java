package com.taller.web.rest;

import com.taller.RentaapApp;

import com.taller.domain.Restaurante;
import com.taller.repository.RestauranteRepository;
import com.taller.service.RestauranteService;
import com.taller.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.taller.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RestauranteResource REST controller.
 *
 * @see RestauranteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentaapApp.class)
public class RestauranteResourceIntTest {

    private static final String DEFAULT_NOMBRE_RESPONSABLE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_RESPONSABLE = "BBBBBBBBBB";

    private static final String DEFAULT_NOMBRE_LOCAL = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_LOCAL = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICACION = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICACION = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IDENTIFICACION = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IDENTIFICACION = "BBBBBBBBBB";

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final Integer DEFAULT_ASIENTOS_TOTALES = 1;
    private static final Integer UPDATED_ASIENTOS_TOTALES = 2;

    private static final Integer DEFAULT_ASIENTOS_DISPONIBLES = 1;
    private static final Integer UPDATED_ASIENTOS_DISPONIBLES = 2;

    @Autowired
    private RestauranteRepository restauranteRepository;

    @Autowired
    private RestauranteService restauranteService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRestauranteMockMvc;

    private Restaurante restaurante;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RestauranteResource restauranteResource = new RestauranteResource(restauranteService);
        this.restRestauranteMockMvc = MockMvcBuilders.standaloneSetup(restauranteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Restaurante createEntity(EntityManager em) {
        Restaurante restaurante = new Restaurante()
            .nombreResponsable(DEFAULT_NOMBRE_RESPONSABLE)
            .nombreLocal(DEFAULT_NOMBRE_LOCAL)
            .identificacion(DEFAULT_IDENTIFICACION)
            .tipoIdentificacion(DEFAULT_TIPO_IDENTIFICACION)
            .direccion(DEFAULT_DIRECCION)
            .telefono(DEFAULT_TELEFONO)
            .asientosTotales(DEFAULT_ASIENTOS_TOTALES)
            .asientosDisponibles(DEFAULT_ASIENTOS_DISPONIBLES);
        return restaurante;
    }

    @Before
    public void initTest() {
        restaurante = createEntity(em);
    }

    @Test
    @Transactional
    public void createRestaurante() throws Exception {
        int databaseSizeBeforeCreate = restauranteRepository.findAll().size();

        // Create the Restaurante
        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isCreated());

        // Validate the Restaurante in the database
        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeCreate + 1);
        Restaurante testRestaurante = restauranteList.get(restauranteList.size() - 1);
        assertThat(testRestaurante.getNombreResponsable()).isEqualTo(DEFAULT_NOMBRE_RESPONSABLE);
        assertThat(testRestaurante.getNombreLocal()).isEqualTo(DEFAULT_NOMBRE_LOCAL);
        assertThat(testRestaurante.getIdentificacion()).isEqualTo(DEFAULT_IDENTIFICACION);
        assertThat(testRestaurante.getTipoIdentificacion()).isEqualTo(DEFAULT_TIPO_IDENTIFICACION);
        assertThat(testRestaurante.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testRestaurante.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRestaurante.getAsientosTotales()).isEqualTo(DEFAULT_ASIENTOS_TOTALES);
        assertThat(testRestaurante.getAsientosDisponibles()).isEqualTo(DEFAULT_ASIENTOS_DISPONIBLES);
    }

    @Test
    @Transactional
    public void createRestauranteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = restauranteRepository.findAll().size();

        // Create the Restaurante with an existing ID
        restaurante.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        // Validate the Restaurante in the database
        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreResponsableIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setNombreResponsable(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNombreLocalIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setNombreLocal(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdentificacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setIdentificacion(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoIdentificacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setTipoIdentificacion(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDireccionIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setDireccion(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelefonoIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setTelefono(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAsientosTotalesIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setAsientosTotales(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAsientosDisponiblesIsRequired() throws Exception {
        int databaseSizeBeforeTest = restauranteRepository.findAll().size();
        // set the field null
        restaurante.setAsientosDisponibles(null);

        // Create the Restaurante, which fails.

        restRestauranteMockMvc.perform(post("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRestaurantes() throws Exception {
        // Initialize the database
        restauranteRepository.saveAndFlush(restaurante);

        // Get all the restauranteList
        restRestauranteMockMvc.perform(get("/api/restaurantes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(restaurante.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreResponsable").value(hasItem(DEFAULT_NOMBRE_RESPONSABLE.toString())))
            .andExpect(jsonPath("$.[*].nombreLocal").value(hasItem(DEFAULT_NOMBRE_LOCAL.toString())))
            .andExpect(jsonPath("$.[*].identificacion").value(hasItem(DEFAULT_IDENTIFICACION.toString())))
            .andExpect(jsonPath("$.[*].tipoIdentificacion").value(hasItem(DEFAULT_TIPO_IDENTIFICACION.toString())))
            .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].asientosTotales").value(hasItem(DEFAULT_ASIENTOS_TOTALES)))
            .andExpect(jsonPath("$.[*].asientosDisponibles").value(hasItem(DEFAULT_ASIENTOS_DISPONIBLES)));
    }
    
    @Test
    @Transactional
    public void getRestaurante() throws Exception {
        // Initialize the database
        restauranteRepository.saveAndFlush(restaurante);

        // Get the restaurante
        restRestauranteMockMvc.perform(get("/api/restaurantes/{id}", restaurante.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(restaurante.getId().intValue()))
            .andExpect(jsonPath("$.nombreResponsable").value(DEFAULT_NOMBRE_RESPONSABLE.toString()))
            .andExpect(jsonPath("$.nombreLocal").value(DEFAULT_NOMBRE_LOCAL.toString()))
            .andExpect(jsonPath("$.identificacion").value(DEFAULT_IDENTIFICACION.toString()))
            .andExpect(jsonPath("$.tipoIdentificacion").value(DEFAULT_TIPO_IDENTIFICACION.toString()))
            .andExpect(jsonPath("$.direccion").value(DEFAULT_DIRECCION.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.asientosTotales").value(DEFAULT_ASIENTOS_TOTALES))
            .andExpect(jsonPath("$.asientosDisponibles").value(DEFAULT_ASIENTOS_DISPONIBLES));
    }

    @Test
    @Transactional
    public void getNonExistingRestaurante() throws Exception {
        // Get the restaurante
        restRestauranteMockMvc.perform(get("/api/restaurantes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRestaurante() throws Exception {
        // Initialize the database
        restauranteService.save(restaurante);

        int databaseSizeBeforeUpdate = restauranteRepository.findAll().size();

        // Update the restaurante
        Restaurante updatedRestaurante = restauranteRepository.findById(restaurante.getId()).get();
        // Disconnect from session so that the updates on updatedRestaurante are not directly saved in db
        em.detach(updatedRestaurante);
        updatedRestaurante
            .nombreResponsable(UPDATED_NOMBRE_RESPONSABLE)
            .nombreLocal(UPDATED_NOMBRE_LOCAL)
            .identificacion(UPDATED_IDENTIFICACION)
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .direccion(UPDATED_DIRECCION)
            .telefono(UPDATED_TELEFONO)
            .asientosTotales(UPDATED_ASIENTOS_TOTALES)
            .asientosDisponibles(UPDATED_ASIENTOS_DISPONIBLES);

        restRestauranteMockMvc.perform(put("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRestaurante)))
            .andExpect(status().isOk());

        // Validate the Restaurante in the database
        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeUpdate);
        Restaurante testRestaurante = restauranteList.get(restauranteList.size() - 1);
        assertThat(testRestaurante.getNombreResponsable()).isEqualTo(UPDATED_NOMBRE_RESPONSABLE);
        assertThat(testRestaurante.getNombreLocal()).isEqualTo(UPDATED_NOMBRE_LOCAL);
        assertThat(testRestaurante.getIdentificacion()).isEqualTo(UPDATED_IDENTIFICACION);
        assertThat(testRestaurante.getTipoIdentificacion()).isEqualTo(UPDATED_TIPO_IDENTIFICACION);
        assertThat(testRestaurante.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testRestaurante.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRestaurante.getAsientosTotales()).isEqualTo(UPDATED_ASIENTOS_TOTALES);
        assertThat(testRestaurante.getAsientosDisponibles()).isEqualTo(UPDATED_ASIENTOS_DISPONIBLES);
    }

    @Test
    @Transactional
    public void updateNonExistingRestaurante() throws Exception {
        int databaseSizeBeforeUpdate = restauranteRepository.findAll().size();

        // Create the Restaurante

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRestauranteMockMvc.perform(put("/api/restaurantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restaurante)))
            .andExpect(status().isBadRequest());

        // Validate the Restaurante in the database
        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRestaurante() throws Exception {
        // Initialize the database
        restauranteService.save(restaurante);

        int databaseSizeBeforeDelete = restauranteRepository.findAll().size();

        // Delete the restaurante
        restRestauranteMockMvc.perform(delete("/api/restaurantes/{id}", restaurante.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Restaurante> restauranteList = restauranteRepository.findAll();
        assertThat(restauranteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Restaurante.class);
        Restaurante restaurante1 = new Restaurante();
        restaurante1.setId(1L);
        Restaurante restaurante2 = new Restaurante();
        restaurante2.setId(restaurante1.getId());
        assertThat(restaurante1).isEqualTo(restaurante2);
        restaurante2.setId(2L);
        assertThat(restaurante1).isNotEqualTo(restaurante2);
        restaurante1.setId(null);
        assertThat(restaurante1).isNotEqualTo(restaurante2);
    }
}
