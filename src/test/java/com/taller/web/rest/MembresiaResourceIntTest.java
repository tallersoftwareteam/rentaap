package com.taller.web.rest;

import com.taller.RentaapApp;

import com.taller.domain.Membresia;
import com.taller.repository.MembresiaRepository;
import com.taller.service.MembresiaService;
import com.taller.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.taller.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MembresiaResource REST controller.
 *
 * @see MembresiaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentaapApp.class)
public class MembresiaResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final String DEFAULT_CODIGO_MEMBRESIA = "AAAAAAAAAA";
    private static final String UPDATED_CODIGO_MEMBRESIA = "BBBBBBBBBB";

    private static final Double DEFAULT_PRECIO = 1D;
    private static final Double UPDATED_PRECIO = 2D;

    @Autowired
    private MembresiaRepository membresiaRepository;

    @Autowired
    private MembresiaService membresiaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMembresiaMockMvc;

    private Membresia membresia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MembresiaResource membresiaResource = new MembresiaResource(membresiaService);
        this.restMembresiaMockMvc = MockMvcBuilders.standaloneSetup(membresiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Membresia createEntity(EntityManager em) {
        Membresia membresia = new Membresia()
            .nombre(DEFAULT_NOMBRE)
            .descripcion(DEFAULT_DESCRIPCION)
            .codigoMembresia(DEFAULT_CODIGO_MEMBRESIA)
            .precio(DEFAULT_PRECIO);
        return membresia;
    }

    @Before
    public void initTest() {
        membresia = createEntity(em);
    }

    @Test
    @Transactional
    public void createMembresia() throws Exception {
        int databaseSizeBeforeCreate = membresiaRepository.findAll().size();

        // Create the Membresia
        restMembresiaMockMvc.perform(post("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membresia)))
            .andExpect(status().isCreated());

        // Validate the Membresia in the database
        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeCreate + 1);
        Membresia testMembresia = membresiaList.get(membresiaList.size() - 1);
        assertThat(testMembresia.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testMembresia.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testMembresia.getCodigoMembresia()).isEqualTo(DEFAULT_CODIGO_MEMBRESIA);
        assertThat(testMembresia.getPrecio()).isEqualTo(DEFAULT_PRECIO);
    }

    @Test
    @Transactional
    public void createMembresiaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = membresiaRepository.findAll().size();

        // Create the Membresia with an existing ID
        membresia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembresiaMockMvc.perform(post("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membresia)))
            .andExpect(status().isBadRequest());

        // Validate the Membresia in the database
        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = membresiaRepository.findAll().size();
        // set the field null
        membresia.setNombre(null);

        // Create the Membresia, which fails.

        restMembresiaMockMvc.perform(post("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membresia)))
            .andExpect(status().isBadRequest());

        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodigoMembresiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = membresiaRepository.findAll().size();
        // set the field null
        membresia.setCodigoMembresia(null);

        // Create the Membresia, which fails.

        restMembresiaMockMvc.perform(post("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membresia)))
            .andExpect(status().isBadRequest());

        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrecioIsRequired() throws Exception {
        int databaseSizeBeforeTest = membresiaRepository.findAll().size();
        // set the field null
        membresia.setPrecio(null);

        // Create the Membresia, which fails.

        restMembresiaMockMvc.perform(post("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membresia)))
            .andExpect(status().isBadRequest());

        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMembresias() throws Exception {
        // Initialize the database
        membresiaRepository.saveAndFlush(membresia);

        // Get all the membresiaList
        restMembresiaMockMvc.perform(get("/api/membresias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membresia.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
            .andExpect(jsonPath("$.[*].codigoMembresia").value(hasItem(DEFAULT_CODIGO_MEMBRESIA.toString())))
            .andExpect(jsonPath("$.[*].precio").value(hasItem(DEFAULT_PRECIO.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getMembresia() throws Exception {
        // Initialize the database
        membresiaRepository.saveAndFlush(membresia);

        // Get the membresia
        restMembresiaMockMvc.perform(get("/api/membresias/{id}", membresia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(membresia.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.codigoMembresia").value(DEFAULT_CODIGO_MEMBRESIA.toString()))
            .andExpect(jsonPath("$.precio").value(DEFAULT_PRECIO.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingMembresia() throws Exception {
        // Get the membresia
        restMembresiaMockMvc.perform(get("/api/membresias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMembresia() throws Exception {
        // Initialize the database
        membresiaService.save(membresia);

        int databaseSizeBeforeUpdate = membresiaRepository.findAll().size();

        // Update the membresia
        Membresia updatedMembresia = membresiaRepository.findById(membresia.getId()).get();
        // Disconnect from session so that the updates on updatedMembresia are not directly saved in db
        em.detach(updatedMembresia);
        updatedMembresia
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .codigoMembresia(UPDATED_CODIGO_MEMBRESIA)
            .precio(UPDATED_PRECIO);

        restMembresiaMockMvc.perform(put("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMembresia)))
            .andExpect(status().isOk());

        // Validate the Membresia in the database
        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeUpdate);
        Membresia testMembresia = membresiaList.get(membresiaList.size() - 1);
        assertThat(testMembresia.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testMembresia.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testMembresia.getCodigoMembresia()).isEqualTo(UPDATED_CODIGO_MEMBRESIA);
        assertThat(testMembresia.getPrecio()).isEqualTo(UPDATED_PRECIO);
    }

    @Test
    @Transactional
    public void updateNonExistingMembresia() throws Exception {
        int databaseSizeBeforeUpdate = membresiaRepository.findAll().size();

        // Create the Membresia

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembresiaMockMvc.perform(put("/api/membresias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membresia)))
            .andExpect(status().isBadRequest());

        // Validate the Membresia in the database
        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMembresia() throws Exception {
        // Initialize the database
        membresiaService.save(membresia);

        int databaseSizeBeforeDelete = membresiaRepository.findAll().size();

        // Delete the membresia
        restMembresiaMockMvc.perform(delete("/api/membresias/{id}", membresia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Membresia> membresiaList = membresiaRepository.findAll();
        assertThat(membresiaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Membresia.class);
        Membresia membresia1 = new Membresia();
        membresia1.setId(1L);
        Membresia membresia2 = new Membresia();
        membresia2.setId(membresia1.getId());
        assertThat(membresia1).isEqualTo(membresia2);
        membresia2.setId(2L);
        assertThat(membresia1).isNotEqualTo(membresia2);
        membresia1.setId(null);
        assertThat(membresia1).isNotEqualTo(membresia2);
    }
}
