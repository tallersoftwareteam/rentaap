import { element, by, ElementFinder } from 'protractor';

export class MembresiaComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-membresia div table .btn-danger'));
    title = element.all(by.css('jhi-membresia div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class MembresiaUpdatePage {
    pageTitle = element(by.id('jhi-membresia-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nombreInput = element(by.id('field_nombre'));
    descripcionInput = element(by.id('field_descripcion'));
    codigoMembresiaInput = element(by.id('field_codigoMembresia'));
    precioInput = element(by.id('field_precio'));
    beneficioSelect = element(by.id('field_beneficio'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNombreInput(nombre) {
        await this.nombreInput.sendKeys(nombre);
    }

    async getNombreInput() {
        return this.nombreInput.getAttribute('value');
    }

    async setDescripcionInput(descripcion) {
        await this.descripcionInput.sendKeys(descripcion);
    }

    async getDescripcionInput() {
        return this.descripcionInput.getAttribute('value');
    }

    async setCodigoMembresiaInput(codigoMembresia) {
        await this.codigoMembresiaInput.sendKeys(codigoMembresia);
    }

    async getCodigoMembresiaInput() {
        return this.codigoMembresiaInput.getAttribute('value');
    }

    async setPrecioInput(precio) {
        await this.precioInput.sendKeys(precio);
    }

    async getPrecioInput() {
        return this.precioInput.getAttribute('value');
    }

    async beneficioSelectLastOption() {
        await this.beneficioSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async beneficioSelectOption(option) {
        await this.beneficioSelect.sendKeys(option);
    }

    getBeneficioSelect(): ElementFinder {
        return this.beneficioSelect;
    }

    async getBeneficioSelectedOption() {
        return this.beneficioSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class MembresiaDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-membresia-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-membresia'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
