/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MembresiaComponentsPage, MembresiaDeleteDialog, MembresiaUpdatePage } from './membresia.page-object';

const expect = chai.expect;

describe('Membresia e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let membresiaUpdatePage: MembresiaUpdatePage;
    let membresiaComponentsPage: MembresiaComponentsPage;
    let membresiaDeleteDialog: MembresiaDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Membresias', async () => {
        await navBarPage.goToEntity('membresia');
        membresiaComponentsPage = new MembresiaComponentsPage();
        await browser.wait(ec.visibilityOf(membresiaComponentsPage.title), 5000);
        expect(await membresiaComponentsPage.getTitle()).to.eq('Membresias');
    });

    it('should load create Membresia page', async () => {
        await membresiaComponentsPage.clickOnCreateButton();
        membresiaUpdatePage = new MembresiaUpdatePage();
        expect(await membresiaUpdatePage.getPageTitle()).to.eq('Create or edit a Membresia');
        await membresiaUpdatePage.cancel();
    });

    it('should create and save Membresias', async () => {
        const nbButtonsBeforeCreate = await membresiaComponentsPage.countDeleteButtons();

        await membresiaComponentsPage.clickOnCreateButton();
        await promise.all([
            membresiaUpdatePage.setNombreInput('nombre'),
            membresiaUpdatePage.setDescripcionInput('descripcion'),
            membresiaUpdatePage.setCodigoMembresiaInput('codigoMembresia'),
            membresiaUpdatePage.setPrecioInput('5'),
            membresiaUpdatePage.beneficioSelectLastOption()
        ]);
        expect(await membresiaUpdatePage.getNombreInput()).to.eq('nombre');
        expect(await membresiaUpdatePage.getDescripcionInput()).to.eq('descripcion');
        expect(await membresiaUpdatePage.getCodigoMembresiaInput()).to.eq('codigoMembresia');
        expect(await membresiaUpdatePage.getPrecioInput()).to.eq('5');
        await membresiaUpdatePage.save();
        expect(await membresiaUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await membresiaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Membresia', async () => {
        const nbButtonsBeforeDelete = await membresiaComponentsPage.countDeleteButtons();
        await membresiaComponentsPage.clickOnLastDeleteButton();

        membresiaDeleteDialog = new MembresiaDeleteDialog();
        expect(await membresiaDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Membresia?');
        await membresiaDeleteDialog.clickOnConfirmButton();

        expect(await membresiaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
