import { element, by, ElementFinder } from 'protractor';

export class SuscripcionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-suscripcion div table .btn-danger'));
    title = element.all(by.css('jhi-suscripcion div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class SuscripcionUpdatePage {
    pageTitle = element(by.id('jhi-suscripcion-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    estadoInput = element(by.id('field_estado'));
    fechaAdquiridaInput = element(by.id('field_fechaAdquirida'));
    fechaVencimientoInput = element(by.id('field_fechaVencimiento'));
    membresiaSelect = element(by.id('field_membresia'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    getEstadoInput() {
        return this.estadoInput;
    }
    async setFechaAdquiridaInput(fechaAdquirida) {
        await this.fechaAdquiridaInput.sendKeys(fechaAdquirida);
    }

    async getFechaAdquiridaInput() {
        return this.fechaAdquiridaInput.getAttribute('value');
    }

    async setFechaVencimientoInput(fechaVencimiento) {
        await this.fechaVencimientoInput.sendKeys(fechaVencimiento);
    }

    async getFechaVencimientoInput() {
        return this.fechaVencimientoInput.getAttribute('value');
    }

    async membresiaSelectLastOption() {
        await this.membresiaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async membresiaSelectOption(option) {
        await this.membresiaSelect.sendKeys(option);
    }

    getMembresiaSelect(): ElementFinder {
        return this.membresiaSelect;
    }

    async getMembresiaSelectedOption() {
        return this.membresiaSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class SuscripcionDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-suscripcion-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-suscripcion'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
