/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SuscripcionComponentsPage, SuscripcionDeleteDialog, SuscripcionUpdatePage } from './suscripcion.page-object';

const expect = chai.expect;

describe('Suscripcion e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let suscripcionUpdatePage: SuscripcionUpdatePage;
    let suscripcionComponentsPage: SuscripcionComponentsPage;
    let suscripcionDeleteDialog: SuscripcionDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load create Suscripcion page', async () => {
        await suscripcionComponentsPage.clickOnCreateButton();
        suscripcionUpdatePage = new SuscripcionUpdatePage();
        expect(await suscripcionUpdatePage.getPageTitle()).to.eq('Create or edit a Suscripcion');
        await suscripcionUpdatePage.cancel();
    });

    it('should create and save Suscripcions', async () => {
        const nbButtonsBeforeCreate = await suscripcionComponentsPage.countDeleteButtons();

        await suscripcionComponentsPage.clickOnCreateButton();
        await promise.all([
            suscripcionUpdatePage.setFechaAdquiridaInput('2000-12-31'),
            suscripcionUpdatePage.setFechaVencimientoInput('2000-12-31'),
            suscripcionUpdatePage.membresiaSelectLastOption()
        ]);
        const selectedEstado = suscripcionUpdatePage.getEstadoInput();
        if (await selectedEstado.isSelected()) {
            await suscripcionUpdatePage.getEstadoInput().click();
            expect(await suscripcionUpdatePage.getEstadoInput().isSelected()).to.be.false;
        } else {
            await suscripcionUpdatePage.getEstadoInput().click();
            expect(await suscripcionUpdatePage.getEstadoInput().isSelected()).to.be.true;
        }
        expect(await suscripcionUpdatePage.getFechaAdquiridaInput()).to.eq('2000-12-31');
        expect(await suscripcionUpdatePage.getFechaVencimientoInput()).to.eq('2000-12-31');
        await suscripcionUpdatePage.save();
        expect(await suscripcionUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await suscripcionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Suscripcion', async () => {
        const nbButtonsBeforeDelete = await suscripcionComponentsPage.countDeleteButtons();
        await suscripcionComponentsPage.clickOnLastDeleteButton();

        suscripcionDeleteDialog = new SuscripcionDeleteDialog();
        expect(await suscripcionDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Suscripcion?');
        await suscripcionDeleteDialog.clickOnConfirmButton();

        expect(await suscripcionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
