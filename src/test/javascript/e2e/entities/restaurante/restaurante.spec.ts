/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { RestauranteComponentsPage, RestauranteDeleteDialog, RestauranteUpdatePage } from './restaurante.page-object';

const expect = chai.expect;

describe('Restaurante e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let restauranteUpdatePage: RestauranteUpdatePage;
    let restauranteComponentsPage: RestauranteComponentsPage;
    let restauranteDeleteDialog: RestauranteDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Restaurantes', async () => {
        await navBarPage.goToEntity('restaurante');
        restauranteComponentsPage = new RestauranteComponentsPage();
        await browser.wait(ec.visibilityOf(restauranteComponentsPage.title), 5000);
        expect(await restauranteComponentsPage.getTitle()).to.eq('Restaurantes');
    });

    it('should load create Restaurante page', async () => {
        await restauranteComponentsPage.clickOnCreateButton();
        restauranteUpdatePage = new RestauranteUpdatePage();
        expect(await restauranteUpdatePage.getPageTitle()).to.eq('Create or edit a Restaurante');
        await restauranteUpdatePage.cancel();
    });

    it('should create and save Restaurantes', async () => {
        const nbButtonsBeforeCreate = await restauranteComponentsPage.countDeleteButtons();

        await restauranteComponentsPage.clickOnCreateButton();
        await promise.all([
            restauranteUpdatePage.setNombreResponsableInput('nombreResponsable'),
            restauranteUpdatePage.setNombreLocalInput('nombreLocal'),
            restauranteUpdatePage.setIdentificacionInput('identificacion'),
            restauranteUpdatePage.setTipoIdentificacionInput('tipoIdentificacion'),
            restauranteUpdatePage.setDireccionInput('direccion'),
            restauranteUpdatePage.setTelefonoInput('telefono'),
            restauranteUpdatePage.setEmailInput('email'),
            restauranteUpdatePage.setPasswordInput('password'),
            restauranteUpdatePage.setAsientosTotalesInput('5'),
            restauranteUpdatePage.setAsientosDisponiblesInput('5')
        ]);
        expect(await restauranteUpdatePage.getNombreResponsableInput()).to.eq('nombreResponsable');
        expect(await restauranteUpdatePage.getNombreLocalInput()).to.eq('nombreLocal');
        expect(await restauranteUpdatePage.getIdentificacionInput()).to.eq('identificacion');
        expect(await restauranteUpdatePage.getTipoIdentificacionInput()).to.eq('tipoIdentificacion');
        expect(await restauranteUpdatePage.getDireccionInput()).to.eq('direccion');
        expect(await restauranteUpdatePage.getTelefonoInput()).to.eq('telefono');
        expect(await restauranteUpdatePage.getEmailInput()).to.eq('email');
        expect(await restauranteUpdatePage.getPasswordInput()).to.eq('password');
        expect(await restauranteUpdatePage.getAsientosTotalesInput()).to.eq('5');
        expect(await restauranteUpdatePage.getAsientosDisponiblesInput()).to.eq('5');
        await restauranteUpdatePage.save();
        expect(await restauranteUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await restauranteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Restaurante', async () => {
        const nbButtonsBeforeDelete = await restauranteComponentsPage.countDeleteButtons();
        await restauranteComponentsPage.clickOnLastDeleteButton();

        restauranteDeleteDialog = new RestauranteDeleteDialog();
        expect(await restauranteDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Restaurante?');
        await restauranteDeleteDialog.clickOnConfirmButton();

        expect(await restauranteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
