import { element, by, ElementFinder } from 'protractor';

export class RestauranteComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-restaurante div table .btn-danger'));
    title = element.all(by.css('jhi-restaurante div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class RestauranteUpdatePage {
    pageTitle = element(by.id('jhi-restaurante-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nombreResponsableInput = element(by.id('field_nombreResponsable'));
    nombreLocalInput = element(by.id('field_nombreLocal'));
    identificacionInput = element(by.id('field_identificacion'));
    tipoIdentificacionInput = element(by.id('field_tipoIdentificacion'));
    direccionInput = element(by.id('field_direccion'));
    telefonoInput = element(by.id('field_telefono'));
    emailInput = element(by.id('field_email'));
    passwordInput = element(by.id('field_password'));
    asientosTotalesInput = element(by.id('field_asientosTotales'));
    asientosDisponiblesInput = element(by.id('field_asientosDisponibles'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNombreResponsableInput(nombreResponsable) {
        await this.nombreResponsableInput.sendKeys(nombreResponsable);
    }

    async getNombreResponsableInput() {
        return this.nombreResponsableInput.getAttribute('value');
    }

    async setNombreLocalInput(nombreLocal) {
        await this.nombreLocalInput.sendKeys(nombreLocal);
    }

    async getNombreLocalInput() {
        return this.nombreLocalInput.getAttribute('value');
    }

    async setIdentificacionInput(identificacion) {
        await this.identificacionInput.sendKeys(identificacion);
    }

    async getIdentificacionInput() {
        return this.identificacionInput.getAttribute('value');
    }

    async setTipoIdentificacionInput(tipoIdentificacion) {
        await this.tipoIdentificacionInput.sendKeys(tipoIdentificacion);
    }

    async getTipoIdentificacionInput() {
        return this.tipoIdentificacionInput.getAttribute('value');
    }

    async setDireccionInput(direccion) {
        await this.direccionInput.sendKeys(direccion);
    }

    async getDireccionInput() {
        return this.direccionInput.getAttribute('value');
    }

    async setTelefonoInput(telefono) {
        await this.telefonoInput.sendKeys(telefono);
    }

    async getTelefonoInput() {
        return this.telefonoInput.getAttribute('value');
    }

    async setEmailInput(email) {
        await this.emailInput.sendKeys(email);
    }

    async getEmailInput() {
        return this.emailInput.getAttribute('value');
    }

    async setPasswordInput(password) {
        await this.passwordInput.sendKeys(password);
    }

    async getPasswordInput() {
        return this.passwordInput.getAttribute('value');
    }

    async setAsientosTotalesInput(asientosTotales) {
        await this.asientosTotalesInput.sendKeys(asientosTotales);
    }

    async getAsientosTotalesInput() {
        return this.asientosTotalesInput.getAttribute('value');
    }

    async setAsientosDisponiblesInput(asientosDisponibles) {
        await this.asientosDisponiblesInput.sendKeys(asientosDisponibles);
    }

    async getAsientosDisponiblesInput() {
        return this.asientosDisponiblesInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class RestauranteDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-restaurante-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-restaurante'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
