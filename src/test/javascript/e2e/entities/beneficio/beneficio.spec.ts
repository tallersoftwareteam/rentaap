/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BeneficioComponentsPage, BeneficioDeleteDialog, BeneficioUpdatePage } from './beneficio.page-object';

const expect = chai.expect;

describe('Beneficio e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let beneficioUpdatePage: BeneficioUpdatePage;
    let beneficioComponentsPage: BeneficioComponentsPage;
    let beneficioDeleteDialog: BeneficioDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Beneficios', async () => {
        await navBarPage.goToEntity('beneficio');
        beneficioComponentsPage = new BeneficioComponentsPage();
        await browser.wait(ec.visibilityOf(beneficioComponentsPage.title), 5000);
        expect(await beneficioComponentsPage.getTitle()).to.eq('Beneficios');
    });

    it('should load create Beneficio page', async () => {
        await beneficioComponentsPage.clickOnCreateButton();
        beneficioUpdatePage = new BeneficioUpdatePage();
        expect(await beneficioUpdatePage.getPageTitle()).to.eq('Create or edit a Beneficio');
        await beneficioUpdatePage.cancel();
    });

    it('should create and save Beneficios', async () => {
        const nbButtonsBeforeCreate = await beneficioComponentsPage.countDeleteButtons();

        await beneficioComponentsPage.clickOnCreateButton();
        await promise.all([beneficioUpdatePage.setDescripcionInput('descripcion')]);
        expect(await beneficioUpdatePage.getDescripcionInput()).to.eq('descripcion');
        await beneficioUpdatePage.save();
        expect(await beneficioUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await beneficioComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Beneficio', async () => {
        const nbButtonsBeforeDelete = await beneficioComponentsPage.countDeleteButtons();
        await beneficioComponentsPage.clickOnLastDeleteButton();

        beneficioDeleteDialog = new BeneficioDeleteDialog();
        expect(await beneficioDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Beneficio?');
        await beneficioDeleteDialog.clickOnConfirmButton();

        expect(await beneficioComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
