import { element, by, ElementFinder } from 'protractor';

export class BeneficioComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-beneficio div table .btn-danger'));
    title = element.all(by.css('jhi-beneficio div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class BeneficioUpdatePage {
    pageTitle = element(by.id('jhi-beneficio-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    descripcionInput = element(by.id('field_descripcion'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setDescripcionInput(descripcion) {
        await this.descripcionInput.sendKeys(descripcion);
    }

    async getDescripcionInput() {
        return this.descripcionInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class BeneficioDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-beneficio-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-beneficio'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
