/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ReservaComponentsPage, ReservaDeleteDialog, ReservaUpdatePage } from './reserva.page-object';

const expect = chai.expect;

describe('Reserva e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let reservaUpdatePage: ReservaUpdatePage;
    let reservaComponentsPage: ReservaComponentsPage;
    let reservaDeleteDialog: ReservaDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Reservas', async () => {
        await navBarPage.goToEntity('reserva');
        reservaComponentsPage = new ReservaComponentsPage();
        await browser.wait(ec.visibilityOf(reservaComponentsPage.title), 5000);
        expect(await reservaComponentsPage.getTitle()).to.eq('Reservas');
    });

    it('should load create Reserva page', async () => {
        await reservaComponentsPage.clickOnCreateButton();
        reservaUpdatePage = new ReservaUpdatePage();
        expect(await reservaUpdatePage.getPageTitle()).to.eq('Create or edit a Reserva');
        await reservaUpdatePage.cancel();
    });

    it('should create and save Reservas', async () => {
        const nbButtonsBeforeCreate = await reservaComponentsPage.countDeleteButtons();

        await reservaComponentsPage.clickOnCreateButton();
        await promise.all([
            reservaUpdatePage.setComentariosInput('comentarios'),
            reservaUpdatePage.setCantidadAsientosInput('5'),
            reservaUpdatePage.setFechaReservaInput('2000-12-31'),
            reservaUpdatePage.membresiaSelectLastOption(),
            reservaUpdatePage.restauranteSelectLastOption()
        ]);
        expect(await reservaUpdatePage.getComentariosInput()).to.eq('comentarios');
        expect(await reservaUpdatePage.getCantidadAsientosInput()).to.eq('5');
        expect(await reservaUpdatePage.getFechaReservaInput()).to.eq('2000-12-31');
        await reservaUpdatePage.save();
        expect(await reservaUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await reservaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Reserva', async () => {
        const nbButtonsBeforeDelete = await reservaComponentsPage.countDeleteButtons();
        await reservaComponentsPage.clickOnLastDeleteButton();

        reservaDeleteDialog = new ReservaDeleteDialog();
        expect(await reservaDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Reserva?');
        await reservaDeleteDialog.clickOnConfirmButton();

        expect(await reservaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
