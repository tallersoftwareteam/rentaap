import { element, by, ElementFinder } from 'protractor';

export class ReservaComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-reserva div table .btn-danger'));
    title = element.all(by.css('jhi-reserva div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ReservaUpdatePage {
    pageTitle = element(by.id('jhi-reserva-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    comentariosInput = element(by.id('field_comentarios'));
    cantidadAsientosInput = element(by.id('field_cantidadAsientos'));
    fechaReservaInput = element(by.id('field_fechaReserva'));
    membresiaSelect = element(by.id('field_membresia'));
    restauranteSelect = element(by.id('field_restaurante'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setComentariosInput(comentarios) {
        await this.comentariosInput.sendKeys(comentarios);
    }

    async getComentariosInput() {
        return this.comentariosInput.getAttribute('value');
    }

    async setCantidadAsientosInput(cantidadAsientos) {
        await this.cantidadAsientosInput.sendKeys(cantidadAsientos);
    }

    async getCantidadAsientosInput() {
        return this.cantidadAsientosInput.getAttribute('value');
    }

    async setFechaReservaInput(fechaReserva) {
        await this.fechaReservaInput.sendKeys(fechaReserva);
    }

    async getFechaReservaInput() {
        return this.fechaReservaInput.getAttribute('value');
    }

    async membresiaSelectLastOption() {
        await this.membresiaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async membresiaSelectOption(option) {
        await this.membresiaSelect.sendKeys(option);
    }

    getMembresiaSelect(): ElementFinder {
        return this.membresiaSelect;
    }

    async getMembresiaSelectedOption() {
        return this.membresiaSelect.element(by.css('option:checked')).getText();
    }

    async restauranteSelectLastOption() {
        await this.restauranteSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async restauranteSelectOption(option) {
        await this.restauranteSelect.sendKeys(option);
    }

    getRestauranteSelect(): ElementFinder {
        return this.restauranteSelect;
    }

    async getRestauranteSelectedOption() {
        return this.restauranteSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ReservaDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-reserva-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-reserva'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
