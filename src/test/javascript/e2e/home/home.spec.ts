/* tslint:disable no-unused-expression */
import { browser, by, element, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../page-objects/jhi-page-objects';

import { HomeComponentPage, SuscripcionUpdateUserPage } from './home.page-object';

const expect = chai.expect;

describe('Home e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let homePage: HomeComponentPage;
    let suscripcionUpdateUserPage: SuscripcionUpdateUserPage;
    let actualSuscription = 0;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('Obteniendo suscripciontes actuales', async () => {
        homePage = new HomeComponentPage();
        await browser.wait(ec.visibilityOf(homePage.chargeMenuMembership), 5000);
        actualSuscription = await homePage.countSuscriptionCards();
    });

    it('Cargar la lista de membresias', async () => {
        await browser.wait(ec.visibilityOf(homePage.chargeMenuMembership), 5000);
        await homePage.clickOnMenuMembershipButton();
    });

    it('Cargar la suscripcion del usuario', async () => {
        await homePage.clickOnCreateButton();
        suscripcionUpdateUserPage = new SuscripcionUpdateUserPage();
        await browser.wait(ec.visibilityOf(suscripcionUpdateUserPage.pageTitle), 5000);
        expect(await suscripcionUpdateUserPage.getPageTitle()).to.eq('Añadir una suscripcion');
    });

    it('Guardando suscripcion', async () => {
        await suscripcionUpdateUserPage.save();
    });

    it('Validando nueva suscripcion', async () => {
        await homePage.clickOnMenuSuscribButton();
        let actual = await homePage.countSuscriptionCards();
        expect(actualSuscription).to.eq(actual - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
