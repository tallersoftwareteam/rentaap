/* tslint:disable no-unused-expression */
import { browser, by, element, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../page-objects/jhi-page-objects';

import { HomeComponentPage, SuscripcionUpdateUserPage } from './home.page-object';

const expect = chai.expect;

describe('Home e2e test-reserva', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let homePage: HomeComponentPage;
    let actualSuscription = 0;
    let restauranteValue = '';

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('jhosep2', 'jhosep2');
        homePage = new HomeComponentPage();
    });

    it('Verificar la lista de restaurantes, seleccionar primero', async () => {
        await homePage.clickOnChargeMenuReservar();
        let restaurants = await homePage.countRestauranteOptions();
        expect(restaurants).to.eq(2);
        await homePage.clickOnRestauranteField();
        await homePage.clickOnRestauranteOption(1);
    });

    it('Evaluando que se muestren los campos despues de la seleccion', async () => {
        await browser.wait(ec.visibilityOf(homePage.reservarHiddenField), 5000);
    });

    it('Evaluando las suscripciones activas', async () => {
        actualSuscription = await homePage.countMembresiasOfUser();
        expect(actualSuscription).equal(3);
    });

    it('realizando reserva con ultima suscripcion en lista', async () => {
        await promise.all([
            homePage.setComentariosInput('comentarios'),
            homePage.setCantidadAsientosInput('5'),
            homePage.setFechaReservaInput('2000-12-31'),
            homePage.membresiaSelectLastOption()
        ]);
        expect(await homePage.getComentariosInput()).to.eq('comentarios');
        expect(await homePage.getCantidadAsientosInput()).to.eq('5');
        expect(await homePage.getFechaReservaInput()).to.eq('2000-12-31');
        await homePage.reservarButtonClick();
        let countAlerts = await homePage.checkErrorField();
        expect(countAlerts).equals(0);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
