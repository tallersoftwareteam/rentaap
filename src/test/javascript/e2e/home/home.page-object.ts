import { browser, element, by, ElementFinder } from 'protractor';

export class HomeComponentPage {
    // Test membresia con id 2101
    chargeMenuMembership = element(by.css('#menu-membresia'));
    chargeMenuSuscription = element(by.css('#menu-suscription'));
    suscriptionCards = element.all(by.css('.card-suscription'));
    createButton = element(by.id('2101'));

    // Test reservar en menu home
    chargeMenuReservar = element(by.css('#menu-reservar'));
    reservarHiddenField = element(by.css('#field_fechaReserva'));
    reservarRestaurantField = element(by.css('select#field_restaurante'));
    reservarRestaurantOptions = element.all(by.css('#field_restaurante option'));
    comentariosInput = element(by.id('field_comentarios'));
    cantidadAsientosInput = element(by.id('field_cantidadAsientos'));
    fechaReservaInput = element(by.id('field_fechaReserva'));
    activeSuscriptionUser = element(by.css('#field_membresia')).all(by.tagName('option'));
    errorTagField = element(by.tagName('div.alerts')).all(by.tagName('div'));
    reservarButton = element(by.id('save-reserva'));

    async countSuscriptionCards() {
        return this.suscriptionCards.count();
    }

    async countMembresiasOfUser() {
        return this.activeSuscriptionUser.count();
    }

    async clickOnMenuMembershipButton() {
        await this.chargeMenuMembership.click();
    }
    async clickOnMenuSuscribButton() {
        await this.chargeMenuSuscription.click();
    }

    async clickOnChargeMenuReservar() {
        await this.chargeMenuReservar.click();
    }

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async countRestauranteOptions() {
        return this.reservarRestaurantOptions.count();
    }

    async clickOnRestauranteField() {
        await this.reservarRestaurantField.click();
    }

    async clickOnRestauranteOption(option) {
        await this.reservarRestaurantOptions.get(option).click();
    }

    async setComentariosInput(comentarios) {
        await this.comentariosInput.sendKeys(comentarios);
    }

    async getComentariosInput() {
        return this.comentariosInput.getAttribute('value');
    }

    async setCantidadAsientosInput(cantidadAsientos) {
        await this.cantidadAsientosInput.sendKeys(cantidadAsientos);
    }

    async getCantidadAsientosInput() {
        return this.cantidadAsientosInput.getAttribute('value');
    }

    async setFechaReservaInput(fechaReserva) {
        await this.fechaReservaInput.sendKeys(fechaReserva);
    }

    async getFechaReservaInput() {
        return this.fechaReservaInput.getAttribute('value');
    }

    async membresiaSelectLastOption() {
        await this.activeSuscriptionUser.last().click();
    }

    async reservarButtonClick() {
        await this.reservarButton.click();
    }

    async checkErrorField() {
        return this.errorTagField.count();
    }
}

export class SuscripcionUpdateUserPage {
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    pageTitle = element(by.id('jhi-suscripcion-heading'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}
