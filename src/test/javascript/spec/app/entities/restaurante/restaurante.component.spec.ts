/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RentaapTestModule } from '../../../test.module';
import { RestauranteComponent } from 'app/entities/restaurante/restaurante.component';
import { RestauranteService } from 'app/entities/restaurante/restaurante.service';
import { Restaurante } from 'app/shared/model/restaurante.model';

describe('Component Tests', () => {
    describe('Restaurante Management Component', () => {
        let comp: RestauranteComponent;
        let fixture: ComponentFixture<RestauranteComponent>;
        let service: RestauranteService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [RentaapTestModule],
                declarations: [RestauranteComponent],
                providers: []
            })
                .overrideTemplate(RestauranteComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RestauranteComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RestauranteService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Restaurante(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.restaurantes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
