/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { RentaapTestModule } from '../../../test.module';
import { MembresiaUpdateComponent } from 'app/entities/membresia/membresia-update.component';
import { MembresiaService } from 'app/entities/membresia/membresia.service';
import { Membresia } from 'app/shared/model/membresia.model';

describe('Component Tests', () => {
    describe('Membresia Management Update Component', () => {
        let comp: MembresiaUpdateComponent;
        let fixture: ComponentFixture<MembresiaUpdateComponent>;
        let service: MembresiaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [RentaapTestModule],
                declarations: [MembresiaUpdateComponent]
            })
                .overrideTemplate(MembresiaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MembresiaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MembresiaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Membresia(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.membresia = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Membresia();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.membresia = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
