/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RentaapTestModule } from '../../../test.module';
import { MembresiaComponent } from 'app/entities/membresia/membresia.component';
import { MembresiaService } from 'app/entities/membresia/membresia.service';
import { Membresia } from 'app/shared/model/membresia.model';

describe('Component Tests', () => {
    describe('Membresia Management Component', () => {
        let comp: MembresiaComponent;
        let fixture: ComponentFixture<MembresiaComponent>;
        let service: MembresiaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [RentaapTestModule],
                declarations: [MembresiaComponent],
                providers: []
            })
                .overrideTemplate(MembresiaComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MembresiaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MembresiaService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Membresia(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.membresias[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
