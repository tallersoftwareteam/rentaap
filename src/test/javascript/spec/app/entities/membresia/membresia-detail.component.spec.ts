/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RentaapTestModule } from '../../../test.module';
import { MembresiaDetailComponent } from 'app/entities/membresia/membresia-detail.component';
import { Membresia } from 'app/shared/model/membresia.model';

describe('Component Tests', () => {
    describe('Membresia Management Detail Component', () => {
        let comp: MembresiaDetailComponent;
        let fixture: ComponentFixture<MembresiaDetailComponent>;
        const route = ({ data: of({ membresia: new Membresia(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [RentaapTestModule],
                declarations: [MembresiaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(MembresiaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(MembresiaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.membresia).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
