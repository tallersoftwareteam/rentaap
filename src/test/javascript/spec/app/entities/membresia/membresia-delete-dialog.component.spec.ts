/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RentaapTestModule } from '../../../test.module';
import { MembresiaDeleteDialogComponent } from 'app/entities/membresia/membresia-delete-dialog.component';
import { MembresiaService } from 'app/entities/membresia/membresia.service';

describe('Component Tests', () => {
    describe('Membresia Management Delete Component', () => {
        let comp: MembresiaDeleteDialogComponent;
        let fixture: ComponentFixture<MembresiaDeleteDialogComponent>;
        let service: MembresiaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [RentaapTestModule],
                declarations: [MembresiaDeleteDialogComponent]
            })
                .overrideTemplate(MembresiaDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(MembresiaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MembresiaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
