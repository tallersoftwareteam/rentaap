import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RentaapSharedModule } from 'app/shared';
import {
    BeneficioComponent,
    BeneficioDetailComponent,
    BeneficioUpdateComponent,
    BeneficioDeletePopupComponent,
    BeneficioDeleteDialogComponent,
    beneficioRoute,
    beneficioPopupRoute
} from './';

const ENTITY_STATES = [...beneficioRoute, ...beneficioPopupRoute];

@NgModule({
    imports: [RentaapSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        BeneficioComponent,
        BeneficioDetailComponent,
        BeneficioUpdateComponent,
        BeneficioDeleteDialogComponent,
        BeneficioDeletePopupComponent
    ],
    entryComponents: [BeneficioComponent, BeneficioUpdateComponent, BeneficioDeleteDialogComponent, BeneficioDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RentaapBeneficioModule {}
