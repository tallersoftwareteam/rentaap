export * from './beneficio.service';
export * from './beneficio-update.component';
export * from './beneficio-delete-dialog.component';
export * from './beneficio-detail.component';
export * from './beneficio.component';
export * from './beneficio.route';
