import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IBeneficio } from 'app/shared/model/beneficio.model';
import { BeneficioService } from './beneficio.service';

@Component({
    selector: 'jhi-beneficio-update',
    templateUrl: './beneficio-update.component.html'
})
export class BeneficioUpdateComponent implements OnInit {
    beneficio: IBeneficio;
    isSaving: boolean;

    constructor(protected beneficioService: BeneficioService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ beneficio }) => {
            this.beneficio = beneficio;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.beneficio.id !== undefined) {
            this.subscribeToSaveResponse(this.beneficioService.update(this.beneficio));
        } else {
            this.subscribeToSaveResponse(this.beneficioService.create(this.beneficio));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBeneficio>>) {
        result.subscribe((res: HttpResponse<IBeneficio>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
