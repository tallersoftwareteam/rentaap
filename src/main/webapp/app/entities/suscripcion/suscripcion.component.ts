import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISuscripcion } from 'app/shared/model/suscripcion.model';
import { AccountService, User } from 'app/core';
import { SuscripcionService } from './suscripcion.service';

@Component({
    selector: 'jhi-suscripcion',
    templateUrl: './suscripcion.component.html'
})
export class SuscripcionComponent implements OnInit, OnDestroy {
    suscripcions: ISuscripcion[];
    currentAccount: any;
    eventSubscriber: Subscription;
    constructor(
        protected suscripcionService: SuscripcionService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.suscripcionService
            .query()
            .pipe(
                filter((res: HttpResponse<ISuscripcion[]>) => res.ok),
                map((res: HttpResponse<ISuscripcion[]>) => res.body)
            )
            .subscribe(
                (res: ISuscripcion[]) => {
                    this.accountService.identity().then(account => {
                        this.currentAccount = Object.assign({}, account);
                        const result = this.currentAccount.authorities.filter(autorite => autorite === 'ROLE_ADMIN');
                        if (result.length === 0) {
                            this.suscripcions = res.filter(suscription => suscription.user.id === this.currentAccount.id);
                        } else {
                            this.suscripcions = res;
                        }
                    });
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.registerChangeInSuscripcions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISuscripcion) {
        return item.id;
    }

    registerChangeInSuscripcions() {
        this.eventSubscriber = this.eventManager.subscribe('suscripcionListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
