import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISuscripcion } from 'app/shared/model/suscripcion.model';

type EntityResponseType = HttpResponse<ISuscripcion>;
type EntityArrayResponseType = HttpResponse<ISuscripcion[]>;

@Injectable({ providedIn: 'root' })
export class SuscripcionService {
    public resourceUrl = SERVER_API_URL + 'api/suscripcions';

    constructor(protected http: HttpClient) {}

    create(suscripcion: ISuscripcion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(suscripcion);
        return this.http
            .post<ISuscripcion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(suscripcion: ISuscripcion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(suscripcion);
        return this.http
            .put<ISuscripcion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ISuscripcion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ISuscripcion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(suscripcion: ISuscripcion): ISuscripcion {
        const copy: ISuscripcion = Object.assign({}, suscripcion, {
            fechaAdquirida:
                suscripcion.fechaAdquirida != null && suscripcion.fechaAdquirida.isValid()
                    ? suscripcion.fechaAdquirida.format(DATE_FORMAT)
                    : null,
            fechaVencimiento:
                suscripcion.fechaVencimiento != null && suscripcion.fechaVencimiento.isValid()
                    ? suscripcion.fechaVencimiento.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.fechaAdquirida = res.body.fechaAdquirida != null ? moment(res.body.fechaAdquirida) : null;
            res.body.fechaVencimiento = res.body.fechaVencimiento != null ? moment(res.body.fechaVencimiento) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((suscripcion: ISuscripcion) => {
                suscripcion.fechaAdquirida = suscripcion.fechaAdquirida != null ? moment(suscripcion.fechaAdquirida) : null;
                suscripcion.fechaVencimiento = suscripcion.fechaVencimiento != null ? moment(suscripcion.fechaVencimiento) : null;
            });
        }
        return res;
    }
}
