import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ISuscripcion } from 'app/shared/model/suscripcion.model';
import { SuscripcionService } from './suscripcion.service';
import { IMembresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from 'app/entities/membresia';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-suscripcion-update',
    templateUrl: './suscripcion-update.component.html'
})
export class SuscripcionUpdateComponent implements OnInit {
    suscripcion: ISuscripcion;
    isSaving: boolean;
    usuarios: IUser[];
    membresias: IMembresia[];
    fechaAdquiridaDp: any;
    fechaVencimientoDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected suscripcionService: SuscripcionService,
        protected membresiaService: MembresiaService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ suscripcion }) => {
            this.suscripcion = suscripcion;
        });
        this.membresiaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMembresia[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMembresia[]>) => response.body)
            )
            .subscribe((res: IMembresia[]) => (this.membresias = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.usuarios = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.suscripcion.id !== undefined) {
            this.subscribeToSaveResponse(this.suscripcionService.update(this.suscripcion));
        } else {
            this.subscribeToSaveResponse(this.suscripcionService.create(this.suscripcion));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISuscripcion>>) {
        result.subscribe((res: HttpResponse<ISuscripcion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackMembresiaById(index: number, item: IMembresia) {
        return item.id;
    }

    trackUsuarioById(index: number, item: IUser) {
        return item.id;
    }
}
