import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ISuscripcion } from 'app/shared/model/suscripcion.model';
import { SuscripcionService } from './suscripcion.service';
import { IMembresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from 'app/entities/membresia';
import { AccountService, IUser, UserService } from 'app/core';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

@Component({
    selector: 'jhi-suscripcion-update-user',
    templateUrl: './suscripcion-update-user.component.html'
})
export class SuscripcionUpdateUserComponent implements OnInit {
    suscripcion: ISuscripcion;
    isSaving: boolean;
    currentUser: IUser;
    currentMembership: IMembresia;
    currentAccount: any;
    fechaAdquiridaDp = moment().format(DATE_FORMAT);
    fechaVencimientoDp = moment()
        .add(30, 'day')
        .format(DATE_FORMAT);

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService,
        protected suscripcionService: SuscripcionService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.currentMembership = JSON.parse(localStorage.getItem('membresia'));
        this.activatedRoute.data.subscribe(({ suscripcion }) => {
            this.suscripcion = suscripcion;
        });

        this.accountService.identity().then(account => {
            this.currentAccount = Object.assign({}, account);
            this.userService
                .find(this.currentAccount.login)
                .subscribe(
                    (res: HttpResponse<IUser>) => (this.currentUser = res.body),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.suscripcion.membresia = this.currentMembership;
        this.suscripcion.estado = true;
        this.suscripcion.fechaVencimiento = moment(this.fechaVencimientoDp);
        this.suscripcion.fechaAdquirida = moment(this.fechaAdquiridaDp);
        this.suscripcion.user = this.currentUser;
        if (this.suscripcion.id !== undefined) {
            this.subscribeToSaveResponse(this.suscripcionService.update(this.suscripcion));
        } else {
            this.subscribeToSaveResponse(this.suscripcionService.create(this.suscripcion));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISuscripcion>>) {
        result.subscribe((res: HttpResponse<ISuscripcion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackMembresiaById(index: number, item: IMembresia) {
        return item.id;
    }

    trackUsuarioById(index: number, item: IUser) {
        return item.id;
    }
}
