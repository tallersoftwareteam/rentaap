import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RentaapSharedModule } from 'app/shared';
import {
    SuscripcionComponent,
    SuscripcionDetailComponent,
    SuscripcionUpdateComponent,
    SuscripcionUpdateUserComponent,
    SuscripcionDeletePopupComponent,
    SuscripcionDeleteDialogComponent,
    suscripcionRoute,
    suscripcionPopupRoute
} from './';

const ENTITY_STATES = [...suscripcionRoute, ...suscripcionPopupRoute];

@NgModule({
    imports: [RentaapSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SuscripcionComponent,
        SuscripcionDetailComponent,
        SuscripcionUpdateComponent,
        SuscripcionDeleteDialogComponent,
        SuscripcionDeletePopupComponent,
        SuscripcionUpdateUserComponent
    ],
    entryComponents: [
        SuscripcionComponent,
        SuscripcionUpdateComponent,
        SuscripcionUpdateUserComponent,
        SuscripcionDeleteDialogComponent,
        SuscripcionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RentaapSuscripcionModule {}
