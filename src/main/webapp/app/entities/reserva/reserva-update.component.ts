import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IReserva } from 'app/shared/model/reserva.model';
import { ReservaService } from './reserva.service';
import { IMembresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from 'app/entities/membresia';
import { IRestaurante } from 'app/shared/model/restaurante.model';
import { RestauranteService } from 'app/entities/restaurante';

@Component({
    selector: 'jhi-reserva-update',
    templateUrl: './reserva-update.component.html'
})
export class ReservaUpdateComponent implements OnInit {
    reserva: IReserva;
    isSaving: boolean;

    membresias: IMembresia[];

    restaurantes: IRestaurante[];
    fechaReservaDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected reservaService: ReservaService,
        protected membresiaService: MembresiaService,
        protected restauranteService: RestauranteService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ reserva }) => {
            this.reserva = reserva;
        });
        this.membresiaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMembresia[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMembresia[]>) => response.body)
            )
            .subscribe((res: IMembresia[]) => (this.membresias = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.restauranteService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IRestaurante[]>) => mayBeOk.ok),
                map((response: HttpResponse<IRestaurante[]>) => response.body)
            )
            .subscribe((res: IRestaurante[]) => (this.restaurantes = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.reserva.id !== undefined) {
            this.subscribeToSaveResponse(this.reservaService.update(this.reserva));
        } else {
            this.subscribeToSaveResponse(this.reservaService.create(this.reserva));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IReserva>>) {
        result.subscribe((res: HttpResponse<IReserva>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackMembresiaById(index: number, item: IMembresia) {
        return item.id;
    }

    trackRestauranteById(index: number, item: IRestaurante) {
        return item.id;
    }
}
