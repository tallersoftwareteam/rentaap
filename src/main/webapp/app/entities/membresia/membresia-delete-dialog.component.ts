import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMembresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from './membresia.service';

@Component({
    selector: 'jhi-membresia-delete-dialog',
    templateUrl: './membresia-delete-dialog.component.html'
})
export class MembresiaDeleteDialogComponent {
    membresia: IMembresia;

    constructor(
        protected membresiaService: MembresiaService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.membresiaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'membresiaListModification',
                content: 'Deleted an membresia'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-membresia-delete-popup',
    template: ''
})
export class MembresiaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ membresia }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(MembresiaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.membresia = membresia;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/membresia', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/membresia', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
