import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IMembresia } from 'app/shared/model/membresia.model';
import { AccountService } from 'app/core';
import { MembresiaService } from './membresia.service';

@Component({
    selector: 'jhi-membresia',
    templateUrl: './membresia.component.html'
})
export class MembresiaComponent implements OnInit, OnDestroy {
    membresias: IMembresia[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected membresiaService: MembresiaService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.membresiaService
            .query()
            .pipe(
                filter((res: HttpResponse<IMembresia[]>) => res.ok),
                map((res: HttpResponse<IMembresia[]>) => res.body)
            )
            .subscribe(
                (res: IMembresia[]) => {
                    this.membresias = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInMembresias();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IMembresia) {
        return item.id;
    }

    registerChangeInMembresias() {
        this.eventSubscriber = this.eventManager.subscribe('membresiaListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
