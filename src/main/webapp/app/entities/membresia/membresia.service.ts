import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMembresia } from 'app/shared/model/membresia.model';

type EntityResponseType = HttpResponse<IMembresia>;
type EntityArrayResponseType = HttpResponse<IMembresia[]>;

@Injectable({ providedIn: 'root' })
export class MembresiaService {
    public resourceUrl = SERVER_API_URL + 'api/membresias';

    constructor(protected http: HttpClient) {}

    create(membresia: IMembresia): Observable<EntityResponseType> {
        return this.http.post<IMembresia>(this.resourceUrl, membresia, { observe: 'response' });
    }

    update(membresia: IMembresia): Observable<EntityResponseType> {
        return this.http.put<IMembresia>(this.resourceUrl, membresia, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IMembresia>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IMembresia[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
