import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMembresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from './membresia.service';
import { IBeneficio } from 'app/shared/model/beneficio.model';
import { BeneficioService } from 'app/entities/beneficio';

@Component({
    selector: 'jhi-membresia-update',
    templateUrl: './membresia-update.component.html'
})
export class MembresiaUpdateComponent implements OnInit {
    membresia: IMembresia;
    isSaving: boolean;

    beneficios: IBeneficio[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected membresiaService: MembresiaService,
        protected beneficioService: BeneficioService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ membresia }) => {
            this.membresia = membresia;
        });
        this.beneficioService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBeneficio[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBeneficio[]>) => response.body)
            )
            .subscribe((res: IBeneficio[]) => (this.beneficios = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.membresia.id !== undefined) {
            this.subscribeToSaveResponse(this.membresiaService.update(this.membresia));
        } else {
            this.subscribeToSaveResponse(this.membresiaService.create(this.membresia));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IMembresia>>) {
        result.subscribe((res: HttpResponse<IMembresia>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBeneficioById(index: number, item: IBeneficio) {
        return item.id;
    }
}
