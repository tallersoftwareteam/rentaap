import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RentaapSharedModule } from 'app/shared';
import {
    MembresiaComponent,
    MembresiaDetailComponent,
    MembresiaUpdateComponent,
    MembresiaDeletePopupComponent,
    MembresiaDeleteDialogComponent,
    membresiaRoute,
    membresiaPopupRoute
} from './';

const ENTITY_STATES = [...membresiaRoute, ...membresiaPopupRoute];

@NgModule({
    imports: [RentaapSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        MembresiaComponent,
        MembresiaDetailComponent,
        MembresiaUpdateComponent,
        MembresiaDeleteDialogComponent,
        MembresiaDeletePopupComponent
    ],
    entryComponents: [MembresiaComponent, MembresiaUpdateComponent, MembresiaDeleteDialogComponent, MembresiaDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RentaapMembresiaModule {}
