import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMembresia } from 'app/shared/model/membresia.model';

@Component({
    selector: 'jhi-membresia-detail',
    templateUrl: './membresia-detail.component.html'
})
export class MembresiaDetailComponent implements OnInit {
    membresia: IMembresia;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ membresia }) => {
            this.membresia = membresia;
        });
    }

    previousState() {
        window.history.back();
    }
}
