import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Membresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from './membresia.service';
import { MembresiaComponent } from './membresia.component';
import { MembresiaDetailComponent } from './membresia-detail.component';
import { MembresiaUpdateComponent } from './membresia-update.component';
import { MembresiaDeletePopupComponent } from './membresia-delete-dialog.component';
import { IMembresia } from 'app/shared/model/membresia.model';

@Injectable({ providedIn: 'root' })
export class MembresiaResolve implements Resolve<IMembresia> {
    constructor(private service: MembresiaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMembresia> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Membresia>) => response.ok),
                map((membresia: HttpResponse<Membresia>) => membresia.body)
            );
        }
        return of(new Membresia());
    }
}

export const membresiaRoute: Routes = [
    {
        path: '',
        component: MembresiaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Membresias'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: MembresiaDetailComponent,
        resolve: {
            membresia: MembresiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Membresias'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: MembresiaUpdateComponent,
        resolve: {
            membresia: MembresiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Membresias'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: MembresiaUpdateComponent,
        resolve: {
            membresia: MembresiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Membresias'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const membresiaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: MembresiaDeletePopupComponent,
        resolve: {
            membresia: MembresiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Membresias'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
