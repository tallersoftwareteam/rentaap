import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'membresia',
                loadChildren: './membresia/membresia.module#RentaapMembresiaModule'
            },
            {
                path: 'reserva',
                loadChildren: './reserva/reserva.module#RentaapReservaModule'
            },
            {
                path: 'restaurante',
                loadChildren: './restaurante/restaurante.module#RentaapRestauranteModule'
            },
            {
                path: 'beneficio',
                loadChildren: './beneficio/beneficio.module#RentaapBeneficioModule'
            },
            {
                path: 'suscripcion',
                loadChildren: './suscripcion/suscripcion.module#RentaapSuscripcionModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RentaapEntityModule {}
