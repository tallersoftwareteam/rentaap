import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IRestaurante } from 'app/shared/model/restaurante.model';
import { RestauranteService } from './restaurante.service';
import { IUser, UserService } from 'app/core';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-restaurante-update',
    templateUrl: './restaurante-update.component.html'
})
export class RestauranteUpdateComponent implements OnInit {
    restaurante: IRestaurante;
    usuarios: IUser[];
    isSaving: boolean;

    constructor(
        protected restauranteService: RestauranteService,
        protected activatedRoute: ActivatedRoute,
        protected userService: UserService,
        protected jhiAlertService: JhiAlertService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ restaurante }) => {
            this.restaurante = restaurante;
        });
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.usuarios = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.restaurante.id !== undefined) {
            this.subscribeToSaveResponse(this.restauranteService.update(this.restaurante));
        } else {
            this.subscribeToSaveResponse(this.restauranteService.create(this.restaurante));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IRestaurante>>) {
        result.subscribe((res: HttpResponse<IRestaurante>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
