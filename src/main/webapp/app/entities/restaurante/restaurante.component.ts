import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRestaurante } from 'app/shared/model/restaurante.model';
import { AccountService } from 'app/core';
import { RestauranteService } from './restaurante.service';

@Component({
    selector: 'jhi-restaurante',
    templateUrl: './restaurante.component.html'
})
export class RestauranteComponent implements OnInit, OnDestroy {
    restaurantes: IRestaurante[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected restauranteService: RestauranteService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.restauranteService
            .query()
            .pipe(
                filter((res: HttpResponse<IRestaurante[]>) => res.ok),
                map((res: HttpResponse<IRestaurante[]>) => res.body)
            )
            .subscribe(
                (res: IRestaurante[]) => {
                    this.restaurantes = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRestaurantes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRestaurante) {
        return item.id;
    }

    registerChangeInRestaurantes() {
        this.eventSubscriber = this.eventManager.subscribe('restauranteListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
