import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRestaurante } from 'app/shared/model/restaurante.model';

@Component({
    selector: 'jhi-restaurante-detail',
    templateUrl: './restaurante-detail.component.html'
})
export class RestauranteDetailComponent implements OnInit {
    restaurante: IRestaurante;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ restaurante }) => {
            this.restaurante = restaurante;
        });
    }

    previousState() {
        window.history.back();
    }
}
