import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IRestaurante } from 'app/shared/model/restaurante.model';
import { RestauranteService } from './restaurante.service';
import { AccountService, IUser, UserService } from 'app/core';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-restaurante-update',
    templateUrl: './restaurante-update-user.component.html'
})
export class RestauranteUpdateUserComponent implements OnInit {
    restaurante: IRestaurante;
    currentAccount: any;
    isSaving: boolean;

    constructor(
        protected restauranteService: RestauranteService,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService,
        protected jhiAlertService: JhiAlertService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.activatedRoute.data.subscribe(({ restaurante }) => {
            this.restaurante = restaurante;
        });
    }

    onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.restaurante.user = this.currentAccount;
        if (this.restaurante.id !== undefined) {
            this.subscribeToSaveResponse(this.restauranteService.update(this.restaurante));
        } else {
            this.subscribeToSaveResponse(this.restauranteService.create(this.restaurante));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IRestaurante>>) {
        result.subscribe((res: HttpResponse<IRestaurante>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
