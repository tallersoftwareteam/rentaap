import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RentaapSharedModule } from 'app/shared';
import {
    RestauranteComponent,
    RestauranteDetailComponent,
    RestauranteUpdateComponent,
    RestauranteUpdateUserComponent,
    RestauranteDeletePopupComponent,
    RestauranteDeleteDialogComponent,
    restauranteRoute,
    restaurantePopupRoute
} from './';

const ENTITY_STATES = [...restauranteRoute, ...restaurantePopupRoute];

@NgModule({
    imports: [RentaapSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RestauranteComponent,
        RestauranteDetailComponent,
        RestauranteUpdateComponent,
        RestauranteUpdateUserComponent,
        RestauranteDeleteDialogComponent,
        RestauranteDeletePopupComponent
    ],
    entryComponents: [
        RestauranteComponent,
        RestauranteUpdateComponent,
        RestauranteUpdateUserComponent,
        RestauranteDeleteDialogComponent,
        RestauranteDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RentaapRestauranteModule {}
