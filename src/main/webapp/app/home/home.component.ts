import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { LoginModalService, AccountService, Account } from 'app/core';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ISuscripcion } from 'app/shared/model/suscripcion.model';
import { SuscripcionService } from 'app/entities/suscripcion';
import { IMembresia, Membresia } from 'app/shared/model/membresia.model';
import { MembresiaService } from 'app/entities/membresia';
import { IReserva, Reserva } from 'app/shared/model/reserva.model';
import { ReservaService } from 'app/entities/reserva';
import { IRestaurante } from 'app/shared/model/restaurante.model';
import { RestauranteService } from 'app/entities/restaurante';

function Comparator(a, b) {
    if (a.precio > b.precio) {
        return 1;
    }
    if (a.precio < b.precio) {
        return -1;
    }
    return 0;
}

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.css']
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    suscripcions: ISuscripcion[];
    membresias: IMembresia[];
    membresiasOfUser: IMembresia[];
    reservas: IReserva[];
    reserva: IReserva;
    haveRestaurant: boolean;
    availablesRestaurantes: IRestaurante[];
    currentAccount: any;
    isCollapsedSuscriptions = false;
    isCollapsedMembership = false;
    isCollapsedReserves = false;
    isLoadingSuscription = false;
    isLoadingMembership = false;

    constructor(
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private suscripcionService: SuscripcionService,
        private membresiaService: MembresiaService,
        private reservaService: ReservaService,
        private restauranteService: RestauranteService
    ) {}

    ngOnInit() {
        this.getStartInformation();
        this.registerAuthenticationSuccess();
    }

    getStartInformation() {
        this.reserva = new Reserva();
        this.membresias = [];
        this.suscripcions = [];
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
        this.suscripcionService
            .query()
            .pipe(
                filter((res: HttpResponse<ISuscripcion[]>) => res.ok),
                map((res: HttpResponse<ISuscripcion[]>) => res.body)
            )
            .subscribe(
                (res: ISuscripcion[]) => {
                    this.accountService.identity().then(account => {
                        this.currentAccount = Object.assign({}, account);
                        const result = this.currentAccount.authorities.filter(autorite => autorite === 'ROLE_ADMIN');

                        if (result.length === 0) {
                            this.suscripcions = res.filter(suscription => suscription.user.id === this.currentAccount.id);
                            this.isLoadingSuscription = false;
                        } else {
                            this.suscripcions = res;
                            this.isLoadingSuscription = false;
                        }
                        this.getMembresiasOfUser();
                    });
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.membresiaService
            .query()
            .pipe(
                filter((res: HttpResponse<IMembresia[]>) => res.ok),
                map((res: HttpResponse<IMembresia[]>) => res.body)
            )
            .subscribe(
                (res: IMembresia[]) => {
                    res = res.sort(Comparator);
                    this.isLoadingMembership = false;
                    this.membresias = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.reservaService
            .query({ membresia: this.membresias })
            .pipe(
                filter((res: HttpResponse<IReserva[]>) => res.ok),
                map((res: HttpResponse<IReserva[]>) => res.body)
            )
            .subscribe(
                (res: IReserva[]) => {
                    this.reservas = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.restauranteService
            .query()
            .pipe(
                filter((res: HttpResponse<IReserva[]>) => res.ok),
                map((res: HttpResponse<IReserva[]>) => res.body)
            )
            .subscribe(
                (res: IRestaurante[]) => {
                    this.availablesRestaurantes = res;
                    this.accountService.identity().then(account => {
                        this.account = account;
                        const rest = this.availablesRestaurantes.filter(restaurant => restaurant.user.login === this.account.login);
                        if (rest.length !== 0) {
                            this.availablesRestaurantes = rest;
                            this.haveRestaurant = true;
                        } else {
                            this.haveRestaurant = false;
                        }
                    });
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.accountService.identity().then(account => {
                this.account = account;
                this.getStartInformation();
            });
        });
    }

    registerMembership(membresia: IMembresia) {
        localStorage.setItem('membresia', JSON.stringify(membresia));
    }

    getMembresiasOfUser() {
        this.membresiasOfUser = this.suscripcions.filter(sus => sus.estado).map(suscripcion => suscripcion.membresia);
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
