import { Route } from '@angular/router';

import { RegisterUserComponent } from './register.user.component';

export const registerUserRoute: Route = {
    path: 'register-user',
    component: RegisterUserComponent,
    data: {
        authorities: [],
        pageTitle: 'Registration'
    }
};
