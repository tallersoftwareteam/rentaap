import { Route } from '@angular/router';

import { RegisterUserRestaurantComponent } from './register-user-restaurant.component';

export const registerUserRestaurantRoute: Route = {
    path: 'register-user-restaurant',
    component: RegisterUserRestaurantComponent,
    data: {
        authorities: [],
        pageTitle: 'Registration'
    }
};
