import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RentaapSharedModule } from 'app/shared';

import {
    PasswordStrengthBarComponent,
    RegisterUserComponent,
    ActivateComponent,
    PasswordComponent,
    RegisterUserRestaurantComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    accountState
} from './';

@NgModule({
    imports: [RentaapSharedModule, RouterModule.forChild(accountState)],
    declarations: [
        ActivateComponent,
        RegisterUserComponent,
        RegisterUserRestaurantComponent,
        PasswordComponent,
        PasswordStrengthBarComponent,
        PasswordResetInitComponent,
        PasswordResetFinishComponent,
        SettingsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RentaapAccountModule {}
