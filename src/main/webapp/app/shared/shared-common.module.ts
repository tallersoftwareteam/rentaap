import { NgModule } from '@angular/core';

import { RentaapSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [RentaapSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [RentaapSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class RentaapSharedCommonModule {}
