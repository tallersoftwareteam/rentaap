import { Moment } from 'moment';
import { IMembresia } from 'app/shared/model/membresia.model';
import { IRestaurante } from 'app/shared/model/restaurante.model';

export interface IReserva {
    id?: number;
    comentarios?: string;
    cantidadAsientos?: number;
    fechaReserva?: Moment;
    membresia?: IMembresia;
    restaurante?: IRestaurante;
    peticiones?: string;
    calificacion?: number;
}

export class Reserva implements IReserva {
    constructor(
        public id?: number,
        public comentarios?: string,
        public cantidadAsientos?: number,
        public calificacion?: number,
        public peticiones?: string,
        public fechaReserva?: Moment,
        public membresia?: IMembresia,
        public restaurante?: IRestaurante
    ) {}
}
