import { ISuscripcion } from 'app/shared/model/suscripcion.model';
import { IReserva } from 'app/shared/model/reserva.model';
import { IBeneficio } from 'app/shared/model/beneficio.model';

export interface IMembresia {
    id?: number;
    nombre?: string;
    descripcion?: string;
    codigoMembresia?: string;
    precio?: number;
    idSuscripcions?: ISuscripcion[];
    idReservas?: IReserva[];
    beneficio?: IBeneficio;
}

export class Membresia implements IMembresia {
    constructor(
        public id?: number,
        public nombre?: string,
        public descripcion?: string,
        public codigoMembresia?: string,
        public precio?: number,
        public idSuscripcions?: ISuscripcion[],
        public idReservas?: IReserva[],
        public beneficio?: IBeneficio
    ) {}
}
