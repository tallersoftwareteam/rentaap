import { IReserva } from 'app/shared/model/reserva.model';
import { IUser } from 'app/core';

export interface IRestaurante {
    id?: number;
    nombreResponsable?: string;
    nombreLocal?: string;
    identificacion?: string;
    tipoIdentificacion?: string;
    direccion?: string;
    telefono?: string;
    asientosTotales?: number;
    asientosDisponibles?: number;
    idReservas?: IReserva[];
    user?: IUser;
}

export class Restaurante implements IRestaurante {
    constructor(
        public id?: number,
        public nombreResponsable?: string,
        public nombreLocal?: string,
        public identificacion?: string,
        public tipoIdentificacion?: string,
        public direccion?: string,
        public telefono?: string,
        public asientosTotales?: number,
        public asientosDisponibles?: number,
        public idReservas?: IReserva[],
        public user?: IUser
    ) {}
}
