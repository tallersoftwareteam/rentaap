import { IMembresia } from 'app/shared/model/membresia.model';

export interface IBeneficio {
    id?: number;
    descripcion?: string;
    idMembresias?: IMembresia[];
}

export class Beneficio implements IBeneficio {
    constructor(public id?: number, public descripcion?: string, public idMembresias?: IMembresia[]) {}
}
