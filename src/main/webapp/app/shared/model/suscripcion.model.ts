import { Moment } from 'moment';
import { IMembresia } from 'app/shared/model/membresia.model';
import { IUser } from 'app/core/user/user.model';

export interface ISuscripcion {
    id?: number;
    estado?: boolean;
    fechaAdquirida?: Moment;
    fechaVencimiento?: Moment;
    membresia?: IMembresia;
    user?: IUser;
}

export class Suscripcion implements ISuscripcion {
    constructor(
        public id?: number,
        public estado?: boolean,
        public fechaAdquirida?: Moment,
        public fechaVencimiento?: Moment,
        public membresia?: IMembresia,
        public user?: IUser
    ) {
        this.estado = this.estado || false;
    }
}
