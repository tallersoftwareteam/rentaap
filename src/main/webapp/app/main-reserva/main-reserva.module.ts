import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { MAIN_RESERVA_ROUTE } from './main-reserva-route';
import { RentaapSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import { MainReservaComponent } from 'app/main-reserva/main-reserva.component';

@NgModule({
    imports: [RentaapSharedModule, RouterModule.forRoot([MAIN_RESERVA_ROUTE])],
    declarations: [MainReservaComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainReservaModule {}
