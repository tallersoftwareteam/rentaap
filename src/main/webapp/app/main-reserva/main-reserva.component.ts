import { Component, OnInit, ViewChild } from '@angular/core';
import { Account, AccountService, LoginModalService } from 'app/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ISuscripcion } from 'app/shared/model/suscripcion.model';
import { IMembresia } from 'app/shared/model/membresia.model';
import { IReserva, Reserva } from 'app/shared/model/reserva.model';
import { IRestaurante } from 'app/shared/model/restaurante.model';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { SuscripcionService } from 'app/entities/suscripcion';
import { MembresiaService } from 'app/entities/membresia';
import { ReservaService } from 'app/entities/reserva';
import { RestauranteService } from 'app/entities/restaurante';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    selector: 'jhi-main-reserva',
    templateUrl: './main-reserva.component.html',
    styles: []
})
export class MainReservaComponent implements OnInit {
    account: Account;
    suscripcions: ISuscripcion[];
    membresias: IMembresia[];
    membresiasOfUser: IMembresia[];
    reservas: IReserva[];
    reserva: IReserva;
    accountRestaurante: IRestaurante;
    availablesRestaurantes: any;
    selectedRestaurante: IRestaurante;
    selectedReserva: IReserva;
    currentAccount: any;
    isOpenRestaurantes = true;
    isCollapsedReserva = false;

    constructor(
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private suscripcionService: SuscripcionService,
        private membresiaService: MembresiaService,
        private reservaService: ReservaService,
        private restauranteService: RestauranteService,
        private modalService: NgbModal
    ) {}

    ngOnInit() {
        this.registerAuthenticationSuccess();
    }

    open(content) {
        this.modalService.open(content);
    }

    getStartInformation() {
        this.reserva = new Reserva();
        this.membresias = [];
        this.suscripcions = [];
        if (this.account && this.account.authorities.find(authority => authority === 'ROLE_USER')) {
            //se consultan todos los restaurantes y se filtran aquellos con asientos disponibles para reservar
            this.restauranteService
                .query()
                .pipe(
                    filter((res: HttpResponse<IReserva[]>) => res.ok),
                    map((res: HttpResponse<IReserva[]>) => res.body)
                )
                .subscribe(
                    (res: IRestaurante[]) => {
                        this.availablesRestaurantes = res;
                        this.availablesRestaurantes = this.availablesRestaurantes.filter(res => res.asientosDisponibles > 0);
                        //se consultan las reservas para obtener promedios
                        this.reservaService
                            .query()
                            .pipe(
                                filter((res: HttpResponse<IReserva[]>) => res.ok),
                                map((res: HttpResponse<IReserva[]>) => res.body)
                            )
                            .subscribe(
                                (res: IReserva[]) => {
                                    this.availablesRestaurantes.forEach(avail => {
                                        let promedio = 0;
                                        let cont = 0;
                                        res.forEach(reserva => {
                                            if (reserva.restaurante.id === avail.id) {
                                                promedio = promedio + reserva.calificacion;
                                                cont++;
                                            }
                                        });
                                        if (cont !== 0) {
                                            avail.calificacion = promedio / cont;
                                        } else {
                                            avail.calificacion = promedio;
                                        }
                                    });
                                    this.orderReservasByFecha();
                                },
                                (res: HttpErrorResponse) => this.onError(res.message)
                            );
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            // se consultan las suscripciones que dispone el usuario para reservar
            this.suscripcionService
                .query()
                .pipe(
                    filter((res: HttpResponse<ISuscripcion[]>) => res.ok),
                    map((res: HttpResponse<ISuscripcion[]>) => res.body)
                )
                .subscribe(
                    (res: ISuscripcion[]) => {
                        this.accountService.identity().then(account => {
                            this.currentAccount = Object.assign({}, account);
                            const result = this.currentAccount.authorities.filter(autorite => autorite === 'ROLE_ADMIN');
                            if (result.length === 0) {
                                this.suscripcions = res.filter(suscription => suscription.user.id === this.currentAccount.id);
                            } else {
                                this.suscripcions = res;
                            }
                            this.getMembresiasOfUser();
                            this.reservaService
                                .query()
                                .pipe(
                                    filter((res: HttpResponse<IReserva[]>) => res.ok),
                                    map((res: HttpResponse<IReserva[]>) => res.body)
                                )
                                .subscribe(
                                    (res: IReserva[]) => {
                                        let acum = [];
                                        this.membresiasOfUser.forEach(mem => {
                                            res.forEach(reserv => {
                                                if (reserv.membresia.id === mem.id) {
                                                    acum.push(reserv);
                                                }
                                            });
                                        });
                                        console.log(acum);
                                        this.reservas = acum;
                                        this.orderReservasByFecha();
                                    },
                                    (res: HttpErrorResponse) => this.onError(res.message)
                                );
                        });
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        } else if (this.account && this.account.authorities.find(authority => authority === 'ROLE_RESTAURANT')) {
            this.restauranteService
                .findByUser(this.account.id)
                .pipe(
                    filter((res: HttpResponse<IRestaurante>) => res.ok),
                    map((res: HttpResponse<IRestaurante>) => res.body)
                )
                .subscribe(
                    (res: IRestaurante) => {
                        this.accountRestaurante = res;
                        this.getReservasForRestaurante();
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    getReservasForRestaurante() {
        //se consultan las reservas que tenga la persona
        this.reservaService
            .query()
            .pipe(
                filter((res: HttpResponse<IReserva[]>) => res.ok),
                map((res: HttpResponse<IReserva[]>) => res.body)
            )
            .subscribe(
                (res: IReserva[]) => {
                    this.reservas = res.filter(value => value.restaurante.id == this.accountRestaurante.id);
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    registerAuthenticationSuccess() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
            this.getStartInformation();
        });
    }

    getMembresiasOfUser() {
        this.membresiasOfUser = this.suscripcions.filter(sus => sus.estado).map(suscripcion => suscripcion.membresia);
    }

    openReservaModal(restaurante: IRestaurante) {
        this.selectedRestaurante = restaurante;
        this.reserva.restaurante = this.selectedRestaurante;
    }

    openReservaComentModal(reserva: IReserva) {
        this.selectedReserva = reserva;
        this.selectedRestaurante = reserva.restaurante;
        this.reserva.restaurante = this.selectedRestaurante;
    }

    resetReserva() {
        this.reserva = new Reserva();
        this.isCollapsedReserva = false;
    }

    calificarReserva() {
        this.reservaService
            .update(this.selectedReserva)
            .subscribe((res: HttpResponse<IReserva>) => this.reservaSuccess(), (res: HttpErrorResponse) => this.onError(res.message));
        this.isCollapsedReserva = false;
        //se actualiza el restaurante
        this.restauranteService
            .update(this.reserva.restaurante)
            .subscribe((res: HttpResponse<IReserva>) => this.reservaSuccess(), (res: HttpErrorResponse) => this.onError(res.message));
        // this.ngOnInit();
    }

    saveReserva() {
        this.reservaService
            .create(this.reserva)
            .subscribe((res: HttpResponse<IReserva>) => this.reservaSuccess(), (res: HttpErrorResponse) => this.onError(res.message));
        this.isCollapsedReserva = false;
        //Se reducen los asientos disponibles
        this.reserva.restaurante.asientosDisponibles = this.reserva.restaurante.asientosDisponibles - this.reserva.cantidadAsientos;
        //se actualiza el restaurante
        this.restauranteService
            .update(this.reserva.restaurante)
            .subscribe((res: HttpResponse<IReserva>) => this.reservaSuccess(), (res: HttpErrorResponse) => this.onError(res.message));

        this.ngOnInit();
    }

    reservaSuccess() {
        this.reserva = new Reserva();
    }

    onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
        open();
    }

    orderReservasByFecha() {
        this.reservas.sort(function(reserva1, reserva2) {
            return reserva1.fechaReserva.date() - reserva2.fechaReserva.date();
        });
    }

    updateAccountRestaurante() {
        this.restauranteService.update(this.accountRestaurante).subscribe(
            (res: HttpResponse<IReserva>) => {
                this.getStartInformation();
                this.isOpenRestaurantes = true;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    deleteReserva(reservaId: IReserva) {
        this.reservaService
            .delete(reservaId.id)
            .subscribe((res: HttpResponse<IReserva>) => this.getStartInformation(), (res: HttpErrorResponse) => this.onError(res.message));
        this.isCollapsedReserva = false;
        //Se incrementan los asientos disponibles
        reservaId.restaurante.asientosDisponibles = reservaId.restaurante.asientosDisponibles + reservaId.cantidadAsientos;
        //se actualiza el restaurante
        this.restauranteService
            .update(reservaId.restaurante)
            .subscribe((res: HttpResponse<IReserva>) => this.getStartInformation(), (res: HttpErrorResponse) => this.onError(res.message));
    }
}
