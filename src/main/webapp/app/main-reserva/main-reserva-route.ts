import { NgModule } from '@angular/core';
import { Route } from '@angular/router';
import { MainReservaComponent } from 'app/main-reserva/main-reserva.component';
import { UserRouteAccessService } from 'app/core';

export const MAIN_RESERVA_ROUTE: Route = {
    path: 'main-reserva',
    component: MainReservaComponent,
    data: {
        authorities: ['ROLE_USER', 'ROLE_RESTAURANT'],
        pageTitle: 'Reservas'
    },
    canActivate: [UserRouteAccessService]
};
