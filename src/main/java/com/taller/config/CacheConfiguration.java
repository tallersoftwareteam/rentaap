package com.taller.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.taller.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.taller.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.taller.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.Reserva.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.Restaurante.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.Beneficio.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".nbs", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".idBeneficios", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".beneficios", jcacheConfiguration);
            cm.createCache(com.taller.domain.Suscripcion.class.getName(), jcacheConfiguration);
            cm.createCache(com.taller.domain.Suscripcion.class.getName() + ".idMembresias", jcacheConfiguration);
            cm.createCache(com.taller.domain.Reserva.class.getName() + ".idRestaurantes", jcacheConfiguration);
            cm.createCache(com.taller.domain.Reserva.class.getName() + ".idReservas", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".ids", jcacheConfiguration);
            cm.createCache(com.taller.domain.Restaurante.class.getName() + ".ids", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".suscripcions", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".reservas", jcacheConfiguration);
            cm.createCache(com.taller.domain.Restaurante.class.getName() + ".reservas", jcacheConfiguration);
            cm.createCache(com.taller.domain.Reserva.class.getName() + ".idMembresias", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".idSuscripcions", jcacheConfiguration);
            cm.createCache(com.taller.domain.Membresia.class.getName() + ".idReservas", jcacheConfiguration);
            cm.createCache(com.taller.domain.Beneficio.class.getName() + ".idMembresias", jcacheConfiguration);
            cm.createCache(com.taller.domain.Restaurante.class.getName() + ".idReservas", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
