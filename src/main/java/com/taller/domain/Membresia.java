package com.taller.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Membresia.
 */
@Entity
@Table(name = "membresia")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Membresia implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @NotNull
    @Column(name = "codigo_membresia", nullable = false)
    private String codigoMembresia;

    @NotNull
    @Column(name = "precio", nullable = false)
    private Double precio;

    @OneToMany(mappedBy = "membresia")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Suscripcion> idSuscripcions = new HashSet<>();

    @OneToMany(mappedBy = "membresia")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Reserva> idReservas = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("idMembresias")
    private Beneficio beneficio;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Membresia nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Membresia descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoMembresia() {
        return codigoMembresia;
    }

    public Membresia codigoMembresia(String codigoMembresia) {
        this.codigoMembresia = codigoMembresia;
        return this;
    }

    public void setCodigoMembresia(String codigoMembresia) {
        this.codigoMembresia = codigoMembresia;
    }

    public Double getPrecio() {
        return precio;
    }

    public Membresia precio(Double precio) {
        this.precio = precio;
        return this;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Set<Suscripcion> getIdSuscripcions() {
        return idSuscripcions;
    }

    public Membresia idSuscripcions(Set<Suscripcion> suscripcions) {
        this.idSuscripcions = suscripcions;
        return this;
    }

    public Membresia addIdSuscripcion(Suscripcion suscripcion) {
        this.idSuscripcions.add(suscripcion);
        suscripcion.setMembresia(this);
        return this;
    }

    public Membresia removeIdSuscripcion(Suscripcion suscripcion) {
        this.idSuscripcions.remove(suscripcion);
        suscripcion.setMembresia(null);
        return this;
    }

    public void setIdSuscripcions(Set<Suscripcion> suscripcions) {
        this.idSuscripcions = suscripcions;
    }

    public Set<Reserva> getIdReservas() {
        return idReservas;
    }

    public Membresia idReservas(Set<Reserva> reservas) {
        this.idReservas = reservas;
        return this;
    }

    public Membresia addIdReserva(Reserva reserva) {
        this.idReservas.add(reserva);
        reserva.setMembresia(this);
        return this;
    }

    public Membresia removeIdReserva(Reserva reserva) {
        this.idReservas.remove(reserva);
        reserva.setMembresia(null);
        return this;
    }

    public void setIdReservas(Set<Reserva> reservas) {
        this.idReservas = reservas;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public Membresia beneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
        return this;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Membresia membresia = (Membresia) o;
        if (membresia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), membresia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Membresia{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", codigoMembresia='" + getCodigoMembresia() + "'" +
            ", precio=" + getPrecio() +
            "}";
    }
}
