package com.taller.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Restaurante.
 */
@Entity
@Table(name = "restaurante")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Restaurante implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nombre_responsable", nullable = false)
    private String nombreResponsable;

    @NotNull
    @Column(name = "nombre_local", nullable = false)
    private String nombreLocal;

    @NotNull
    @Column(name = "identificacion", nullable = false)
    private String identificacion;

    @NotNull
    @Column(name = "tipo_identificacion", nullable = false)
    private String tipoIdentificacion;

    @NotNull
    @Column(name = "direccion", nullable = false)
    private String direccion;

    @NotNull
    @Column(name = "telefono", nullable = false)
    private String telefono;

    @NotNull
    @Column(name = "asientos_totales", nullable = false)
    private Integer asientosTotales;

    @NotNull
    @Column(name = "asientos_disponibles", nullable = false)
    private Integer asientosDisponibles;

    @ManyToOne
    @JsonIgnoreProperties("idRestaurante")
    private User user;

    @OneToMany(mappedBy = "restaurante")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Reserva> idReservas = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreResponsable() {
        return nombreResponsable;
    }

    public Restaurante nombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
        return this;
    }

    public void setNombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
    }

    public String getNombreLocal() {
        return nombreLocal;
    }

    public Restaurante nombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
        return this;
    }

    public void setNombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public Restaurante identificacion(String identificacion) {
        this.identificacion = identificacion;
        return this;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public Restaurante tipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
        return this;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public Restaurante direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public Restaurante telefono(String telefono) {
        this.telefono = telefono;
        return this;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getAsientosTotales() {
        return asientosTotales;
    }

    public Restaurante asientosTotales(Integer asientosTotales) {
        this.asientosTotales = asientosTotales;
        return this;
    }

    public void setAsientosTotales(Integer asientosTotales) {
        this.asientosTotales = asientosTotales;
    }

    public Integer getAsientosDisponibles() {
        return asientosDisponibles;
    }

    public Restaurante asientosDisponibles(Integer asientosDisponibles) {
        this.asientosDisponibles = asientosDisponibles;
        return this;
    }

    public void setAsientosDisponibles(Integer asientosDisponibles) {
        this.asientosDisponibles = asientosDisponibles;
    }

    public User getUser() {
        return user;
    }

    public Restaurante user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Restaurante restaurante = (Restaurante) o;
        if (restaurante.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), restaurante.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Restaurante{" +
            "id=" + getId() +
            ", nombreResponsable='" + getNombreResponsable() + "'" +
            ", nombreLocal='" + getNombreLocal() + "'" +
            ", identificacion='" + getIdentificacion() + "'" +
            ", tipoIdentificacion='" + getTipoIdentificacion() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", telefono='" + getTelefono() + "'" +
            ", asientosTotales=" + getAsientosTotales() +
            ", asientosDisponibles=" + getAsientosDisponibles() +
            "}";
    }
}
