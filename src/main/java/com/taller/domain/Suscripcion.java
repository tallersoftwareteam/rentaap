package com.taller.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Suscripcion.
 */
@Entity
@Table(name = "suscripcion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Suscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "estado", nullable = false)
    private Boolean estado;

    @NotNull
    @Column(name = "fecha_adquirida", nullable = false)
    private LocalDate fechaAdquirida;

    @NotNull
    @Column(name = "fecha_vencimiento", nullable = false)
    private LocalDate fechaVencimiento;

    @ManyToOne
    @JsonIgnoreProperties("idSuscripcions")
    private Membresia membresia;

    @ManyToOne
    @JsonIgnoreProperties("idSuscripcions")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEstado() {
        return estado;
    }

    public Suscripcion estado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public LocalDate getFechaAdquirida() {
        return fechaAdquirida;
    }

    public Suscripcion fechaAdquirida(LocalDate fechaAdquirida) {
        this.fechaAdquirida = fechaAdquirida;
        return this;
    }

    public void setFechaAdquirida(LocalDate fechaAdquirida) {
        this.fechaAdquirida = fechaAdquirida;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public Suscripcion fechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
        return this;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Membresia getMembresia() {
        return membresia;
    }

    public Suscripcion membresia(Membresia membresia) {
        this.membresia = membresia;
        return this;
    }

    public void setMembresia(Membresia membresia) {
        this.membresia = membresia;
    }

    public User getUser() {
        return user;
    }

    public Suscripcion user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Suscripcion suscripcion = (Suscripcion) o;
        if (suscripcion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), suscripcion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Suscripcion{" +
            "id=" + getId() +
            ", estado='" + isEstado() + "'" +
            ", fechaAdquirida='" + getFechaAdquirida() + "'" +
            ", fechaVencimiento='" + getFechaVencimiento() + "'" +
            "}";
    }
}
