package com.taller.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Reserva.
 */
@Entity
@Table(name = "reserva")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "comentarios")
    private String comentarios;

    @Column(name = "peticiones")
    private String peticiones;

    @Column(name = "calificacion")
    private int calificacion;

    @NotNull
    @Column(name = "cantidad_asientos", nullable = false)
    private Integer cantidadAsientos;

    @NotNull
    @Column(name = "fecha_reserva", nullable = false)
    private LocalDate fechaReserva;

    @ManyToOne
    @JsonIgnoreProperties("idReservas")
    private Membresia membresia;

    @ManyToOne
    @JsonIgnoreProperties("idReservas")
    private Restaurante restaurante;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComentarios() {
        return comentarios;
    }

    public Reserva comentarios(String comentarios) {
        this.comentarios = comentarios;
        return this;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getPeticiones() {
        return peticiones;
    }

    public Reserva peticiones(String peticiones) {
        this.peticiones = peticiones;
        return this;
    }

    public void setPeticiones(String peticiones) {
        this.peticiones = peticiones;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public Reserva calificacion(int calificacion) {
        this.calificacion = calificacion;
        return this;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getCantidadAsientos() {
        return cantidadAsientos;
    }

    public Reserva cantidadAsientos(Integer cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
        return this;
    }

    public void setCantidadAsientos(Integer cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
    }

    public LocalDate getFechaReserva() {
        return fechaReserva;
    }

    public Reserva fechaReserva(LocalDate fechaReserva) {
        this.fechaReserva = fechaReserva;
        return this;
    }

    public void setFechaReserva(LocalDate fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public Membresia getMembresia() {
        return membresia;
    }

    public Reserva membresia(Membresia membresia) {
        this.membresia = membresia;
        return this;
    }

    public void setMembresia(Membresia membresia) {
        this.membresia = membresia;
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }

    public Reserva restaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
        return this;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reserva reserva = (Reserva) o;
        if (reserva.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reserva.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reserva{" +
            "id=" + getId() +
            ", comentarios='" + getComentarios() + "'" +
            ", cantidadAsientos=" + getCantidadAsientos() +
            ", fechaReserva='" + getFechaReserva() + "'" +
            ", peticiones='" + getPeticiones() + "'" +
            ", calificacion='" + getCalificacion() + "'" +
            "}";
    }
}
