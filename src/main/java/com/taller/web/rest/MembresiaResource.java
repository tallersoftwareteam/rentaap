package com.taller.web.rest;
import com.taller.domain.Membresia;
import com.taller.service.MembresiaService;
import com.taller.web.rest.errors.BadRequestAlertException;
import com.taller.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Membresia.
 */
@RestController
@RequestMapping("/api")
public class MembresiaResource {

    private final Logger log = LoggerFactory.getLogger(MembresiaResource.class);

    private static final String ENTITY_NAME = "membresia";

    private final MembresiaService membresiaService;

    public MembresiaResource(MembresiaService membresiaService) {
        this.membresiaService = membresiaService;
    }

    /**
     * POST  /membresias : Create a new membresia.
     *
     * @param membresia the membresia to create
     * @return the ResponseEntity with status 201 (Created) and with body the new membresia, or with status 400 (Bad Request) if the membresia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/membresias")
    public ResponseEntity<Membresia> createMembresia(@Valid @RequestBody Membresia membresia) throws URISyntaxException {
        log.debug("REST request to save Membresia : {}", membresia);
        if (membresia.getId() != null) {
            throw new BadRequestAlertException("A new membresia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Membresia result = membresiaService.save(membresia);
        return ResponseEntity.created(new URI("/api/membresias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /membresias : Updates an existing membresia.
     *
     * @param membresia the membresia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated membresia,
     * or with status 400 (Bad Request) if the membresia is not valid,
     * or with status 500 (Internal Server Error) if the membresia couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/membresias")
    public ResponseEntity<Membresia> updateMembresia(@Valid @RequestBody Membresia membresia) throws URISyntaxException {
        log.debug("REST request to update Membresia : {}", membresia);
        if (membresia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Membresia result = membresiaService.save(membresia);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, membresia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /membresias : get all the membresias.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of membresias in body
     */
    @GetMapping("/membresias")
    public List<Membresia> getAllMembresias() {
        log.debug("REST request to get all Membresias");
        return membresiaService.findAll();
    }

    /**
     * GET  /membresias/:id : get the "id" membresia.
     *
     * @param id the id of the membresia to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the membresia, or with status 404 (Not Found)
     */
    @GetMapping("/membresias/{id}")
    public ResponseEntity<Membresia> getMembresia(@PathVariable Long id) {
        log.debug("REST request to get Membresia : {}", id);
        Optional<Membresia> membresia = membresiaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(membresia);
    }

    /**
     * DELETE  /membresias/:id : delete the "id" membresia.
     *
     * @param id the id of the membresia to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/membresias/{id}")
    public ResponseEntity<Void> deleteMembresia(@PathVariable Long id) {
        log.debug("REST request to delete Membresia : {}", id);
        membresiaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
