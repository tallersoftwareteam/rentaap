/**
 * View Models used by Spring MVC REST controllers.
 */
package com.taller.web.rest.vm;
