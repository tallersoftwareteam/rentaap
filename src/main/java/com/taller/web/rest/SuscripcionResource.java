package com.taller.web.rest;
import com.taller.domain.Suscripcion;
import com.taller.service.SuscripcionService;
import com.taller.web.rest.errors.BadRequestAlertException;
import com.taller.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Suscripcion.
 */
@RestController
@RequestMapping("/api")
public class SuscripcionResource {

    private final Logger log = LoggerFactory.getLogger(SuscripcionResource.class);

    private static final String ENTITY_NAME = "suscripcion";

    private final SuscripcionService suscripcionService;

    public SuscripcionResource(SuscripcionService suscripcionService) {
        this.suscripcionService = suscripcionService;
    }

    /**
     * POST  /suscripcions : Create a new suscripcion.
     *
     * @param suscripcion the suscripcion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new suscripcion, or with status 400 (Bad Request) if the suscripcion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suscripcions")
    public ResponseEntity<Suscripcion> createSuscripcion(@Valid @RequestBody Suscripcion suscripcion) throws URISyntaxException {
        log.debug("REST request to save Suscripcion : {}", suscripcion);
        if (suscripcion.getId() != null) {
            throw new BadRequestAlertException("A new suscripcion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Suscripcion result = suscripcionService.save(suscripcion);
        return ResponseEntity.created(new URI("/api/suscripcions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /suscripcions : Updates an existing suscripcion.
     *
     * @param suscripcion the suscripcion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated suscripcion,
     * or with status 400 (Bad Request) if the suscripcion is not valid,
     * or with status 500 (Internal Server Error) if the suscripcion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/suscripcions")
    public ResponseEntity<Suscripcion> updateSuscripcion(@Valid @RequestBody Suscripcion suscripcion) throws URISyntaxException {
        log.debug("REST request to update Suscripcion : {}", suscripcion);
        if (suscripcion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Suscripcion result = suscripcionService.save(suscripcion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, suscripcion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /suscripcions : get all the suscripcions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of suscripcions in body
     */
    @GetMapping("/suscripcions")
    public List<Suscripcion> getAllSuscripcions() {
        log.debug("REST request to get all Suscripcions");
        return suscripcionService.findAll();
    }

    /**
     * GET  /suscripcions/:id : get the "id" suscripcion.
     *
     * @param id the id of the suscripcion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the suscripcion, or with status 404 (Not Found)
     */
    @GetMapping("/suscripcions/{id}")
    public ResponseEntity<Suscripcion> getSuscripcion(@PathVariable Long id) {
        log.debug("REST request to get Suscripcion : {}", id);
        Optional<Suscripcion> suscripcion = suscripcionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(suscripcion);
    }

    /**
     * DELETE  /suscripcions/:id : delete the "id" suscripcion.
     *
     * @param id the id of the suscripcion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/suscripcions/{id}")
    public ResponseEntity<Void> deleteSuscripcion(@PathVariable Long id) {
        log.debug("REST request to delete Suscripcion : {}", id);
        suscripcionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
