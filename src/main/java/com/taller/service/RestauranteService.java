package com.taller.service;

import com.taller.domain.Restaurante;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Restaurante.
 */
public interface RestauranteService {

    /**
     * Save a restaurante.
     *
     * @param restaurante the entity to save
     * @return the persisted entity
     */
    Restaurante save(Restaurante restaurante);

    /**
     * Get all the restaurantes.
     *
     * @return the list of entities
     */
    List<Restaurante> findAll();


    /**
     * Get the "id" restaurante.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Restaurante> findOne(Long id);

    /**
     * Delete the "id" restaurante.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
