package com.taller.service.impl;

import com.taller.service.RestauranteService;
import com.taller.domain.Restaurante;
import com.taller.repository.RestauranteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Restaurante.
 */
@Service
@Transactional
public class RestauranteServiceImpl implements RestauranteService {

    private final Logger log = LoggerFactory.getLogger(RestauranteServiceImpl.class);

    private final RestauranteRepository restauranteRepository;

    public RestauranteServiceImpl(RestauranteRepository restauranteRepository) {
        this.restauranteRepository = restauranteRepository;
    }

    /**
     * Save a restaurante.
     *
     * @param restaurante the entity to save
     * @return the persisted entity
     */
    @Override
    public Restaurante save(Restaurante restaurante) {
        log.debug("Request to save Restaurante : {}", restaurante);
        return restauranteRepository.save(restaurante);
    }

    /**
     * Get all the restaurantes.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Restaurante> findAll() {
        log.debug("Request to get all Restaurantes");
        return restauranteRepository.findAll();
    }


    /**
     * Get one restaurante by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Restaurante> findOne(Long id) {
        log.debug("Request to get Restaurante : {}", id);
        return restauranteRepository.findById(id);
    }

    /**
     * Delete the restaurante by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Restaurante : {}", id);
        restauranteRepository.deleteById(id);
    }
}
