package com.taller.service.impl;

import com.taller.service.SuscripcionService;
import com.taller.domain.Suscripcion;
import com.taller.repository.SuscripcionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Suscripcion.
 */
@Service
@Transactional
public class SuscripcionServiceImpl implements SuscripcionService {

    private final Logger log = LoggerFactory.getLogger(SuscripcionServiceImpl.class);

    private final SuscripcionRepository suscripcionRepository;

    public SuscripcionServiceImpl(SuscripcionRepository suscripcionRepository) {
        this.suscripcionRepository = suscripcionRepository;
    }

    /**
     * Save a suscripcion.
     *
     * @param suscripcion the entity to save
     * @return the persisted entity
     */
    @Override
    public Suscripcion save(Suscripcion suscripcion) {
        log.debug("Request to save Suscripcion : {}", suscripcion);
        return suscripcionRepository.save(suscripcion);
    }

    /**
     * Get all the suscripcions.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Suscripcion> findAll() {
        log.debug("Request to get all Suscripcions");
        return suscripcionRepository.findAll();
    }


    /**
     * Get one suscripcion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Suscripcion> findOne(Long id) {
        log.debug("Request to get Suscripcion : {}", id);
        return suscripcionRepository.findById(id);
    }

    /**
     * Delete the suscripcion by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Suscripcion : {}", id);
        suscripcionRepository.deleteById(id);
    }
}
