package com.taller.service.impl;

import com.taller.service.MembresiaService;
import com.taller.domain.Membresia;
import com.taller.repository.MembresiaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Membresia.
 */
@Service
@Transactional
public class MembresiaServiceImpl implements MembresiaService {

    private final Logger log = LoggerFactory.getLogger(MembresiaServiceImpl.class);

    private final MembresiaRepository membresiaRepository;

    public MembresiaServiceImpl(MembresiaRepository membresiaRepository) {
        this.membresiaRepository = membresiaRepository;
    }

    /**
     * Save a membresia.
     *
     * @param membresia the entity to save
     * @return the persisted entity
     */
    @Override
    public Membresia save(Membresia membresia) {
        log.debug("Request to save Membresia : {}", membresia);
        return membresiaRepository.save(membresia);
    }

    /**
     * Get all the membresias.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Membresia> findAll() {
        log.debug("Request to get all Membresias");
        return membresiaRepository.findAll();
    }


    /**
     * Get one membresia by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Membresia> findOne(Long id) {
        log.debug("Request to get Membresia : {}", id);
        return membresiaRepository.findById(id);
    }

    /**
     * Delete the membresia by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Membresia : {}", id);
        membresiaRepository.deleteById(id);
    }
}
