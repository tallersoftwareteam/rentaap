package com.taller.service;

import com.taller.domain.Reserva;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Reserva.
 */
public interface ReservaService {

    /**
     * Save a reserva.
     *
     * @param reserva the entity to save
     * @return the persisted entity
     */
    Reserva save(Reserva reserva);

    /**
     * Get all the reservas.
     *
     * @return the list of entities
     */
    List<Reserva> findAll();


    /**
     * Get the "id" reserva.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Reserva> findOne(Long id);

    /**
     * Delete the "id" reserva.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
