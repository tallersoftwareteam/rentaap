package com.taller.service;

import com.taller.domain.Suscripcion;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Suscripcion.
 */
public interface SuscripcionService {

    /**
     * Save a suscripcion.
     *
     * @param suscripcion the entity to save
     * @return the persisted entity
     */
    Suscripcion save(Suscripcion suscripcion);

    /**
     * Get all the suscripcions.
     *
     * @return the list of entities
     */
    List<Suscripcion> findAll();


    /**
     * Get the "id" suscripcion.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Suscripcion> findOne(Long id);

    /**
     * Delete the "id" suscripcion.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
