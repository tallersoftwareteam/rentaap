package com.taller.service;

import com.taller.domain.Membresia;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Membresia.
 */
public interface MembresiaService {

    /**
     * Save a membresia.
     *
     * @param membresia the entity to save
     * @return the persisted entity
     */
    Membresia save(Membresia membresia);

    /**
     * Get all the membresias.
     *
     * @return the list of entities
     */
    List<Membresia> findAll();


    /**
     * Get the "id" membresia.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Membresia> findOne(Long id);

    /**
     * Delete the "id" membresia.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
